<?php

$testdata = (strtr($_GET['state'], '-_~', '+/='));

if (base64_encode(base64_decode($testdata)) === $testdata) {
    $redirectto = base64_decode($testdata);
} else {
    $redirectto = urldecode($_GET['state']);
}

$params_raw = $_GET;
if (isset($params_raw['scope'])) {
    unset($params_raw['scope']);
}

$params = http_build_query($params_raw);

$url = $redirectto.'&'.$params;

header('location: '.$url);
die();
