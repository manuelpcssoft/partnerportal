<?php

namespace TheLion\ShareoneDrive;

class Mediaplayer
{
    /**
     * @var \TheLion\ShareoneDrive\Processor
     */
    private $_processor;

    public function __construct(Processor $_processor)
    {
        $this->_processor = $_processor;
    }

    /**
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor()
    {
        return $this->_processor;
    }

    public function getMediaList()
    {
        $this->_folder = $this->get_processor()->get_client()->get_folder();

        if ((false === $this->_folder)) {
            exit();
        }

        $sub_entries = $this->get_processor()->get_client()->get_folder_recursive($this->_folder['folder']);
        $this->_folder['contents'] = array_merge($sub_entries, $this->_folder['contents']);
        $this->mediaarray = $this->createMediaArray();

        if (count($this->mediaarray) > 0) {
            $response = json_encode($this->mediaarray);

            $cached_request = new CacheRequest($this->get_processor());
            $cached_request->add_cached_response($response);
            echo $response;
        }

        exit();
    }

    public function setFolder($folder)
    {
        $this->_folder = $folder;
    }

    public function createMediaArray()
    {
        $covers = [];
        $captions = [];

        // Add covers and Captions
        if (count($this->_folder['contents']) > 0) {
            foreach ($this->_folder['contents'] as $key => $node) {
                $child = $node->get_entry();

                if (!isset($child->extension)) {
                    continue;
                }

                if (in_array(strtolower($child->extension), ['png', 'jpg', 'jpeg'])) {
                    // Add images to cover array
                    $covers[$child->get_basename()] = $child;
                    unset($this->_folder['contents'][$key]);
                } elseif ('vtt' === strtolower($child->extension)) {
                    /**
                     * VTT files are supported for captions:.
                     *
                     * Filename: Videoname.Caption Label.Language.VTT
                     */
                    $caption_values = explode('.', $child->get_basename());

                    if (3 !== count($caption_values)) {
                        continue;
                    }

                    $video_name = $caption_values[0];

                    if (!isset($captions[$video_name])) {
                        $captions[$video_name] = [];
                    }

                    $captions[$video_name][] = [
                        'label' => $caption_values[1],
                        'language' => $caption_values[2],
                        'src' => SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-stream&id='.$child->get_id().'&dl=1&caption=1&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$node->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken(),
                    ];

                    unset($this->_folder['contents'][$key]);
                }
            }
        }

        $files = [];

        //Create Filelist array
        if (count($this->_folder['contents']) > 0) {
            $foldername = $this->_folder['folder']->get_entry()->get_name();

            foreach ($this->_folder['contents'] as $node) {
                $child = $node->get_entry();

                if (false === $this->is_media_file($node)) {
                    continue;
                }

                // Check if entry is allowed
                if (!$this->get_processor()->_is_entry_authorized($node)) {
                    continue;
                }

                $basename = $child->get_basename();
                $extension = $child->get_extension();

                $thumb_width = 100;
                $thumb_height = '';
                // FIX: Business accounts thumbnail links not working with empty height value
                if ($child->get_media('height') > 0 && $child->get_media('width') > 0) {
                    $thumb_height = round(($child->get_media('height') / $child->get_media('width')) * $thumb_width);
                }

                if (isset($covers[$basename])) {
                    $poster = $covers[$basename]->get_thumbnail_large();
                    $thumbnailsmall = $covers[$basename]->get_thumbnail_with_size($thumb_height, $thumb_width);
                } elseif (isset($covers[$foldername])) {
                    $poster = $covers[$foldername]->get_thumbnail_large();
                    $thumbnailsmall = $covers[$foldername]->get_thumbnail_with_size($thumb_height, $thumb_width);
                } else {
                    $poster = $child->get_thumbnail_large();
                    $thumbnailsmall = $child->get_thumbnail_with_size($thumb_height, $thumb_width);
                }

                $use_ID3 = '1' === $this->get_processor()->get_shortcode_option('id3') && (null !== $child->get_media('title'));

                $folder_str = dirname($node->get_path($this->_folder['folder']->get_id()));
                $folder_str = trim(str_replace('\\', '/', $folder_str), '/');
                $path = $folder_str.$basename;

                // combine same files with different extensions
                if (!isset($files[$path])) {
                    $source_url = SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-stream&id='.$child->get_id().'&dl=1&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$node->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken();
                    if (('Yes' !== $this->get_processor()->get_setting('google_analytics'))) {
                        $cached_source_url = get_transient('shareonedrive_stream_'.$child->get_id().'_'.$child->get_extension());
                        if (false !== $cached_source_url && false === filter_var($cached_source_url, FILTER_VALIDATE_URL)) {
                            $source_url = $cached_source_url;
                        }
                    }

                    if ($use_ID3) {
                        $track = (null !== $child->get_media('track')) ? $child->get_media('track').'. ' : '';
                        $title = $track.$child->get_media('title');
                        $album = $child->get_media('album');
                        $album = (!empty($album)) ? $child->get_media('album').' &#8226; ' : '';
                        $artist = (null !== $child->get_media('artist')) ? $child->get_media('artist') : '';
                    } else {
                        $title = $child->get_basename();
                        $album = '';
                        $artist = $child->get_description();
                    }

                    $last_edited = $child->get_last_edited();

                    $files[$path] = [
                        'title' => $title,
                        'name' => $basename,
                        'artist' => $album.$artist,
                        'is_dir' => false,
                        'size' => $child->get_size(),
                        'last_edited' => $last_edited,
                        'last_edited_date_str' => !empty($last_edited) ? get_date_from_gmt(date('Y-m-d H:i:s', $last_edited), get_option('date_format')) : '',
                        'last_edited_time_str' => !empty($last_edited) ? get_date_from_gmt(date('Y-m-d H:i:s', $last_edited), get_option('time_format')) : '',
                        'created' => $child->get_created_time(),
                        'created_str' => $child->get_created_time_str(),
                        'folder' => $folder_str,
                        'poster' => $poster,
                        'thumb' => $thumbnailsmall,
                        'download' => (('1' === $this->get_processor()->get_shortcode_option('linktomedia')) && $this->get_processor()->get_user()->can_download()) ? str_replace('shareonedrive-stream', 'shareonedrive-download', $source_url) : false,
                        'source' => $source_url,
                        'captions' => isset($captions[$basename]) ? $captions[$basename] : [],
                        'type' => Helpers::get_mimetype($extension),
                        'width' => $child->get_media('width'),
                        'duration' => $child->get_media('duration') / 1000, //ms to sec,
                        'linktoshop' => ('' !== $this->get_processor()->get_shortcode_option('linktoshop')) ? $this->get_processor()->get_shortcode_option('linktoshop') : false,
                    ];
                }
            }

            $files = $this->get_processor()->sort_filelist($files);
        }

        if ('-1' !== $this->get_processor()->get_shortcode_option('max_files')) {
            $files = array_slice($files, 0, $this->get_processor()->get_shortcode_option('max_files'));
        }

        return array_values($files);
    }

    public function is_media_file(CacheNode $node)
    {
        $entry = $node->get_entry();

        if ($entry->is_dir()) {
            return false;
        }

        $extension = $entry->get_extension();
        $mimetype = $entry->get_mimetype();

        if ('audio' === $this->get_processor()->get_shortcode_option('mode')) {
            $allowedextensions = ['mp3', 'm4a', 'ogg', 'oga', 'wav'];
            $allowedimimetypes = ['audio/mpeg', 'audio/mp4', 'audio/ogg', 'audio/x-wav'];
        } else {
            $allowedextensions = ['mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'webm'];
            $allowedimimetypes = ['video/mp4', 'video/ogg', 'video/webm'];
        }

        if (!empty($extension) && in_array($extension, $allowedextensions)) {
            return true;
        }

        return in_array($mimetype, $allowedimimetypes);
    }
}
