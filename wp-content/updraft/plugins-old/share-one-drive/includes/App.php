<?php

namespace TheLion\ShareoneDrive;

class App
{
    /**
     * @var string
     */
    private $_account_type = '';

    /**
     * @var bool
     */
    private $_own_app = false;

    /**
     * @var string
     */
    private $_app_key = '655f6a98-e3b6-490f-8ebd-e1a6714471d4';

    /**
     * @var string
     */
    private $_app_secret = 'egj7GeOds1z7a8G8CGXXmek';

    /**
     * @var string
     */
    private $_identifier;

    /**
     * @var \SODOneDrive_Service_Drive
     */
    private $_onedrive_drive_service;

    /**
     * @var \SODOneDrive_Service_Sites
     */
    private $_onedrive_sites_service;

    /**
     * @var \SODOneDrive_Service_User
     */
    private $_onedrive_user_service;

    /**
     * @var \SODOneDrive_Client
     */
    private $_client;

    /**
     * We don't save your data or share it.
     * This script just simply creates a redirect with your id and secret to OneDrive and returns the created token.
     * It is exactly the same script as the _authorizeApp.php file in the includes folder of the plugin,
     * and is used for an easy and one-click authorization process that will always work!
     *
     * @var string
     */
    private $_auth_url = 'https://www.wpcloudplugins.com/share-one-drive/index.php';

    /**
     * @var string
     */
    private $_redirect_uri;

    /**
     * @var \TheLion\ShareoneDrive\Processor
     */
    private $_processor;

    public function __construct(Processor $processor)
    {
        $this->_processor = $processor;

        $this->_token_location = SHAREONEDRIVE_CACHEDIR.'/'.get_current_blog_id().'.access_token';
        if ($this->_processor->is_network_authorized()) {
            $this->_token_location = SHAREONEDRIVE_CACHEDIR.'/network.access_token';
        }

        // Call back for refresh token function in SDK client
        add_action('share-one-drive-refresh-token', [&$this, 'refresh_token'], 10, 1);

        if (!function_exists('onedrive_api_php_client_autoload')) {
            require_once SHAREONEDRIVE_ROOTDIR.'/vendors/API/autoload.php';
        }

        $own_key = $this->get_processor()->get_setting('onedrive_app_client_id');
        $own_secret = $this->get_processor()->get_setting('onedrive_app_client_secret');

        if (
                (!empty($own_key))
                && (!empty($own_secret))
        ) {
            $this->_app_key = $this->get_processor()->get_setting('onedrive_app_client_id');
            $this->_app_secret = $this->get_processor()->get_setting('onedrive_app_client_secret');
            $this->_own_app = true;
        }

        // Set right redirect URL
        $this->set_redirect_uri();
    }

    public function process_authorization()
    {
        $this->get_processor()->reset_complete_cache(true);

        if (isset($_GET['code'])) {
            $access_token = $this->create_access_token();
            // Close oAuth popup and refresh admin page. Only possible with inline javascript.
            echo '<script type="text/javascript">window.opener.parent.location.href = "'.$this->get_redirect_uri().'"; window.close();</script>';

            exit();
        }

        return false;
    }

    public function can_do_own_auth()
    {
        return true;
    }

    public function has_plugin_own_app()
    {
        return $this->_own_app;
    }

    public function get_auth_url()
    {
        return $this->get_client()->createAuthUrl();

        return $this->_auth_url;
    }

    /**
     * @return \SODOneDrive_Client
     */
    public function start_client(Account $account = null)
    {
        try {
            $this->_client = new \SODOneDrive_Client();
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Cannot start OneDrive Client %s', $ex->getMessage()));

            return $ex;
        }

        $this->_client->setClientId($this->get_app_key());
        $this->_client->setClientSecret($this->get_app_secret());
        $this->_client->setRedirectUri($this->_auth_url);
        $this->_client->setApprovalPrompt('none');
        $this->_client->setAccessType('offline');

        $this->_client->setScopes([
            'offline_access',
            'files.readwrite.all',
            'sites.readwrite.all',
            'user.read', ]);

        if ($this->get_processor()->is_network_authorized()) {
            $state = network_admin_url('admin.php?page=ShareoneDrive_network_settings&action=shareonedrive_authorization&network=1');
        } else {
            $state = admin_url('admin.php?page=ShareoneDrive_settings&action=shareonedrive_authorization');
        }

        $this->_client->setState(strtr(base64_encode($state), '+/=', '-_~'));

        $this->set_logger();

        if (null === $account) {
            return $this->_client;
        }

        $authorization = $account->get_authorization();

        if (false === $authorization->has_access_token()) {
            return $this->_client;
        }

        $access_token = $authorization->get_access_token();

        if (empty($access_token)) {
            return $this->_client;
        }

        $this->_client->setAccessToken($access_token);

        // Check if the AccessToken is still valid
        if (false === $this->_client->isAccessTokenExpired()) {
            return $this->_client;
        }

        // If we end up here, we have to refresh the token
        return $this->refresh_token($account);
    }

    public function refresh_token(Account $account = null)
    {
        $authorization = $account->get_authorization();
        $access_token = $authorization->get_access_token();

        if (!flock($authorization->get_token_file_handle(), LOCK_EX | LOCK_NB)) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Wait till another process has renewed the Authorization Token'));

            /*
             * If the file cannot be unlocked and the last time
             * it was modified was 1 minute, assume that
             * the previous process died and unlock the file manually
             */
            $requires_unlock = ((filemtime($authorization->get_token_location()) + 60) < (time()));

            // Temporarily workaround when flock is disabled. Can cause problems when plugin is used in multiple processes
            if (false !== strpos(ini_get('disable_functions'), 'flock')) {
                $requires_unlock = false;
            }

            if ($requires_unlock) {
                $authorization->unlock_token_file();
            }

            if (flock($authorization->get_token_file_handle(), LOCK_SH)) {
                clearstatcache();
                rewind($authorization->get_token_file_handle());
                $access_token = fread($authorization->get_token_file_handle(), filesize($authorization->get_token_location()));
                error_log('[WP Cloud Plugin message]: '.sprintf('New Authorization Token has been received by another process.'));
                $this->_client->setAccessToken($access_token);
                $authorization->unlock_token_file();

                return $this->_client;
            }
        }

        //error_log('[WP Cloud Plugin message]: ' . sprintf('Start renewing the Authorization Token'));

        // Stop if we need to get a new AccessToken but somehow ended up without a refreshtoken
        $refresh_token = $this->_client->getRefreshToken();

        if (empty($refresh_token)) {
            error_log('[WP Cloud Plugin message]: '.sprintf('No Refresh Token found during the renewing of the current token. We will stop the authorization completely.'));
            $authorization->set_is_valid(false);
            $authorization->unlock_token_file();
            $this->revoke_token($account);

            return false;
        }

        // Refresh token
        try {
            $this->_client->refreshToken($refresh_token);

            // Store the new token
            $new_accestoken = $this->_client->getAccessToken();
            $authorization->set_access_token($new_accestoken);
            $authorization->unlock_token_file();

            //error_log('[WP Cloud Plugin message]: ' . sprintf('Received new Authorization Token'));

            if (false !== ($timestamp = wp_next_scheduled('shareonedrive_lost_authorisation_notification', ['account_id' => $account->get_id()]))) {
                wp_unschedule_event($timestamp, 'shareonedrive_lost_authorisation_notification', ['account_id' => $account->get_id()]);
            }
        } catch (\Exception $ex) {
            $authorization->set_is_valid(false);
            $authorization->unlock_token_file();
            error_log('[WP Cloud Plugin message]: '.sprintf('Cannot refresh Authorization Token'));

            if (!wp_next_scheduled('shareonedrive_lost_authorisation_notification', ['account_id' => $account->get_id()])) {
                wp_schedule_event(time(), 'daily', 'shareonedrive_lost_authorisation_notification', ['account_id' => $account->get_id()]);
            }

            $this->get_processor()->reset_complete_cache(true);

            throw $ex;
        }

        return $this->_client;
    }

    public function set_logger()
    {
        if ('Yes' === $this->get_processor()->get_setting('api_log')) {
            // Logger
            $this->get_client()->setClassConfig('SODOneDrive_Logger_File', [
                'file' => SHAREONEDRIVE_CACHEDIR.'/api.log',
                'mode' => 0640,
                'lock' => true, ]);

            $this->get_client()->setClassConfig('SODOneDrive_Logger_Abstract', [
                'level' => 'debug', //'warning' or 'debug'
                'log_format' => "[%datetime%] %level%: %message% %context%\n",
                'date_format' => 'd/M/Y:H:i:s O',
                'allow_newlines' => true, ]);

            $this->get_client()->setLogger(new \SODOneDrive_Logger_File($this->get_client()));
        }
    }

    public function create_access_token()
    {
        $this->get_processor()->reset_complete_cache();

        try {
            $code = $_REQUEST['code'];
            $state = $_REQUEST['state'];

            //Fetch the AccessToken
            $this->get_client()->authenticate($code);
            $access_token = $this->get_client()->getAccessToken();

            // Get & Update User Information
            $account_data = $this->get_user()->me->get();
            $drive_data = $this->get_drive()->about->get();

            $account = new Account(
                $account_data->getId(),
                $account_data->getDisplayName(),
                $account_data->getUserPrincipalName(),
                $drive_data->getDriveType()
            );

            $account->get_authorization()->set_access_token($access_token);
            $account->get_authorization()->unlock_token_file();
            $this->get_accounts()->add_account($account);

            delete_transient('shareonedrive_'.$account->get_id().'_is_authorized');
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Cannot generate Access Token: %s', $ex->getMessage()));

            return new \WP_Error('broke', esc_html__('Error communicating with API:', 'wpcloudplugins').$ex->getMessage());
        }

        try {
            // Generate Image for business accounts
            if ('personal' !== $account->get_type()) {
                $photo_info = $this->get_user()->me->photometa('48x48');
                $content_type = $photo_info['@odata.mediaContentType'];

                $photo_data = $this->get_user()->me->photo('48x48', ['alt' => 'media']);
                $image = "data:{$content_type};base64,".base64_encode($photo_data);
                $account->set_image($image);
                $this->get_accounts()->add_account($account);
            }
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Cannot obtain profile photo: %s', $ex->getMessage()));
        }

        return true;
    }

    public function revoke_token(Account $account)
    {
        error_log('[WP Cloud Plugin message]: '.'Lost authorization');

        // Reset Private Folders Back-End if the account it is pointing to is deleted
        $private_folders_data = $this->get_processor()->get_setting('userfolder_backend_auto_root', []);
        if (is_array($private_folders_data) && isset($private_folders_data['account']) && $private_folders_data['account'] === $account->get_id()) {
            $this->get_processor()->set_setting('userfolder_backend_auto_root', null);
        }

        $this->get_processor()->reset_complete_cache(true);

        if (false !== ($timestamp = wp_next_scheduled('shareonedrive_lost_authorisation_notification', ['account_id' => $account->get_id()]))) {
            wp_unschedule_event($timestamp, 'shareonedrive_lost_authorisation_notification', ['account_id' => $account->get_id()]);
        }

        $this->get_processor()->get_main()->send_lost_authorisation_notification($account->get_id());

        try {
            // No Endpoint in the Graph API to revoke tokens
            // $this->get_client()->revokeToken();
            $this->get_accounts()->remove_account($account->get_id());
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.$ex->getMessage());
        }

        delete_transient('shareonedrive_'.$account->get_id().'_is_authorized');

        return true;
    }

    public function get_app_key()
    {
        return $this->_app_key;
    }

    public function get_app_secret()
    {
        return $this->_app_secret;
    }

    public function set_app_key($_app_key)
    {
        $this->_app_key = $_app_key;
    }

    public function set_app_secret($_app_secret)
    {
        $this->_app_secret = $_app_secret;
    }

    /**
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor()
    {
        return $this->_processor;
    }

    /**
     * @return \SODOneDrive_Client
     */
    public function get_client()
    {
        if (empty($this->_client)) {
            $this->_client = $this->start_client();
        }

        return $this->_client;
    }

    /**
     * @return \SODOneDrive_Service_Drive
     */
    public function get_drive()
    {
        if (empty($this->_onedrive_drive_service)) {
            $client = $this->get_client();
            $this->_onedrive_drive_service = new \SODOneDrive_Service_Drive($client);
        }

        return $this->_onedrive_drive_service;
    }

    /**
     * @return \SODOneDrive_Service_Sites
     */
    public function get_sites()
    {
        if (empty($this->_onedrive_sites_service)) {
            $client = $this->get_client();
            $this->_onedrive_sites_service = new \SODOneDrive_Service_Sites($client);
        }

        return $this->_onedrive_sites_service;
    }

    /**
     * @return \TheLion\ShareoneDrive\Accounts
     */
    public function get_accounts()
    {
        return $this->get_processor()->get_main()->get_accounts();
    }

    /**
     * @return \SODOneDrive_Service_User
     */
    public function get_user()
    {
        if (empty($this->_onedrive_user_service)) {
            $client = $this->get_client();
            $this->_onedrive_user_service = new \SODOneDrive_Service_User($client);
        }

        return $this->_onedrive_user_service;
    }

    public function get_redirect_uri()
    {
        return $this->_redirect_uri;
    }

    public function set_redirect_uri()
    {
        $this->_redirect_uri = admin_url('admin.php?page=ShareoneDrive_settings');
        if (isset($_GET['network'])) {
            $this->_redirect_uri = network_admin_url('admin.php?page=ShareoneDrive_network_settings');
        }
    }
}
