<?php

namespace TheLion\ShareoneDrive\Integrations;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

class GravityPDF
{
    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        if (false === get_option('gfpdf_current_version') && false === class_exists('GFPDF_Core')) {
            return;
        }

        add_action('gfpdf_post_save_pdf', [$this, 'shareonedrive_post_save_pdf'], 10, 5);
        add_filter('gfpdf_form_settings_advanced', [$this, 'shareonedrive_add_pdf_setting'], 10, 1);
    }

    /*
         * GravityPDF
         * Basic configuration in Form Settings -> PDF:
         *
         * Always Save PDF = YES
         * [ONEDRIVE] Export PDF = YES
         * [ONEDRIVE] ID = ID where the PDFs need to be stored
         */

    public function shareonedrive_add_pdf_setting($fields)
    {
        $fields['shareonedrive_save_to_onedrive'] = [
            'id' => 'shareonedrive_save_to_onedrive',
            'name' => '[ONEDRIVE] Export PDF',
            'desc' => 'Save the created PDF to OneDrive',
            'type' => 'radio',
            'options' => [
                'Yes' => esc_html__('Yes'),
                'No' => esc_html__('No'),
            ],
            'std' => esc_html__('No'),
        ];

        global $ShareoneDrive;

        $main_account = $ShareoneDrive->get_accounts()->get_primary_account();

        $account_id = '';
        if (!empty($main_account)) {
            $account_id = $main_account->get_id();
        }

        $fields['shareonedrive_save_to_account_id'] = [
            'id' => 'shareonedrive_save_to_account_id',
            'name' => '[ONEDRIVE] Account ID',
            'desc' => 'Account ID where the PDFs need to be stored. E.g. <code>'.$account_id.'</code>',
            'type' => 'text',
            'std' => $account_id,
        ];

        $drive_id = $ShareoneDrive->get_processor()->get_primary_drive_id();

        $fields['shareonedrive_save_to_drive_id'] = [
            'id' => 'shareonedrive_save_to_drive_id',
            'name' => '[ONEDRIVE] Drive ID',
            'desc' => 'Drive ID where the PDFs need to be stored. E.g. <code>'.$drive_id.'</code>',
            'type' => 'text',
            'std' => $drive_id,
        ];

        $fields['shareonedrive_save_to_onedrive_id'] = [
            'id' => 'shareonedrive_save_to_onedrive_id',
            'name' => '[ONEDRIVE] Folder ID',
            'desc' => 'Folder ID where the PDFs need to be stored. E.g. <code>64FA552F!1192</code> or <code>01EXLASDFSD54PWSELRRZ</code>',
            'type' => 'text',
            'std' => '',
        ];

        return $fields;
    }

    public function shareonedrive_post_save_pdf($pdf_path, $filename, $settings, $entry, $form)
    {
        global $ShareoneDrive;
        $processor = $ShareoneDrive->get_processor();

        if (!isset($settings['shareonedrive_save_to_onedrive']) || 'No' === $settings['shareonedrive_save_to_onedrive']) {
            return false;
        }

        $file = [
            'tmp_path' => $pdf_path,
            'type' => mime_content_type($pdf_path),
            'name' => $entry['id'].'-'.$filename,
        ];

        if (!isset($settings['shareonedrive_save_to_account_id'])) {
            // Fall back for older PDF configurations
            $settings['shareonedrive_save_to_account_id'] = $ShareoneDrive->get_accounts()->get_primary_account()->get_id();
        }

        if (!isset($settings['shareonedrive_save_to_drive_id'])) {
            // Fall back for older PDF configurations
            $settings['shareonedrive_save_to_drive_id'] = $ShareoneDrive->get_processor()->get_primary_drive_id();
        }

        $account_id = apply_filters('shareonedrive_gravitypdf_set_account_id', $settings['shareonedrive_save_to_account_id'], $settings, $entry, $form, $processor);
        $drive_id = apply_filters('shareonedrive_gravitypdf_set_drive_id', $settings['shareonedrive_save_to_drive_id'], $settings, $entry, $form, $processor);
        $folder_id = apply_filters('shareonedrive_gravitypdf_set_folder_id', $settings['shareonedrive_save_to_onedrive_id'], $settings, $entry, $form, $processor);

        return $this->shareonedrive_upload_gravify_pdf($file, $account_id, $drive_id, $folder_id);
    }

    public function shareonedrive_upload_gravify_pdf($file, $account_id, $drive_id, $folder_id)
    {
        global $ShareoneDrive;
        $processor = $ShareoneDrive->get_processor();

        $requested_account = $processor->get_accounts()->get_account_by_id($account_id);
        if (null !== $requested_account) {
            $processor->set_current_account($requested_account);
        } else {
            error_log(sprintf("[WP Cloud Plugin message]: OneDrive account (ID: %s) as it isn't linked with the plugin", $account_id));

            exit();
        }

        // Write file
        $chunkSizeBytes = 20 * 320 * 1000; // Multiple of 320kb, the recommended fragment size is between 5-10 MB.

        $processor->get_app()->get_client()->setDefer(true);

        // Create new OneDrive File
        $body = [
            'item' => [
                '@microsoft.graph.conflictBehavior' => 'replace',
            ],
        ];

        try {
            $request = $processor->get_app()->get_drive()->items->upload($file['name'], $folder_id, $body, ['driveId' => $drive_id]);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Not uploaded to the cloud on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }

        // Create a media file upload to represent our upload process.
        $media = new \SODOneDrive_Http_MediaFileUpload($processor->get_app()->get_client(), $request, null, null, true, $chunkSizeBytes);

        $filesize = filesize($file['tmp_path']);
        $media->setFileSize($filesize);

        try {
            $upload_status = false;
            $bytesup = 0;
            $handle = fopen($file['tmp_path'], 'rb');
            while (!$upload_status && !feof($handle)) {
                @set_time_limit(60);
                $chunk = fread($handle, $chunkSizeBytes);
                $upload_status = $media->nextChunk($chunk);
                $bytesup += $chunkSizeBytes;
            }

            fclose($handle);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Not uploaded to the cloud on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }

        $processor->get_app()->get_client()->setDefer(false);
    }
}

 new GravityPDF();
