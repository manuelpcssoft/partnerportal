'use strict';

window.init_share_one_drive_media_player = function (listtoken) {
  var container = document.querySelector('.media[data-token="' + listtoken + '"]');

  /* Load Playlist via Ajax */
  var data = {
    action: 'shareonedrive-get-playlist',
    account_id: container.getAttribute('data-account-id'),
    drive_id: container.getAttribute('data-drive-id'),
    lastFolder: container.getAttribute('data-id'),
    sort: container.getAttribute('data-sort'),
    listtoken: listtoken,
    _ajax_nonce: ShareoneDrive_vars.getplaylist_nonce
  };

  jQuery.ajaxQueue({
    type: "POST",
    url: ShareoneDrive_vars.ajax_url,
    data: data,
    success: function (data) {
      var playlist = create_playlistfrom_json(data);
      init_mediaelement(container, listtoken, playlist);
    },
    error: function () {
      container.querySelector('.loading.initialize').style.display = 'none';
      container.querySelector('.wpcp__main-container').classList.add('error');
    },
    dataType: 'json'
  });
}