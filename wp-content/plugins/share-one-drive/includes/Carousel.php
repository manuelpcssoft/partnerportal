<?php

namespace TheLion\ShareoneDrive;

class Carousel
{
    /**
     * @var \TheLion\ShareoneDrive\Processor
     */
    private $_processor;

    public function __construct(Processor $_processor)
    {
        $this->_processor = $_processor;
    }

    /**
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor()
    {
        return $this->_processor;
    }

    public function get_images_list()
    {
        $this->_folder = $this->get_processor()->get_client()->get_folder();

        if ((false === $this->_folder)) {
        }

        $images = $this->get_images();
        $data = [
            'images' => $images,
            'total' => count($images),
        ];

        if ($data['total'] > 0) {
            $response = json_encode($data);

            $cached_request = new \TheLion\ShareoneDrive\CacheRequest($this->get_processor());
            $cached_request->add_cached_response($response);
            echo $response;
        }

        die();
    }

    public function get_images()
    {
        $entries = $this->get_processor()->get_client()->get_folder_recursive($this->_folder['folder']);
        //$entries = array_merge($subfolders, $this->_folder['contents']);

        $images = [];

        $date_format = get_option('date_format').' '.get_option('time_format');

        foreach ($entries as $entry_id => $cached_entry) {
            $entry = $cached_entry->get_entry();
            if ($entry->is_dir()) {
                continue;
            }

            $unixtime = $entry->get_media('time');

            $images[] = [
                'id' => $entry_id,
                'width' => $entry->get_media('width'),
                'height' => $entry->get_media('height'),
                'timestamp' => $unixtime,
                'date' => date_i18n($date_format, $unixtime),
                'url' => $entry->get_thumbnail_large(),
                'preloaded' => false,
            ];
        }

        //asort($images);
        //foreach ($images as $key => $node) {
        //    $timestamps[$key] = $node['timestamp'];
        //}

        //array_multisort($timestamps, SORT_ASC, $images);

        return $images;
    }
}
