<?php

namespace TheLion\ShareoneDrive;

class Client
{
    public $apifilefields = 'thumbnails(select=c48x48,medium,large,c1500x1500),children(expand=thumbnails(select=c48x48,medium,large,c1500x1500))';
    public $apifilefieldsexpire = 'thumbnails(select=c48x48,medium,large,c1500x1500),children(expand=thumbnails(select=c48x48,medium,large,c1500x1500))';
    public $apilistfilesfields = 'thumbnails(select=c48x48,medium,large,c1500x1500)';
    public $apilistfilesexpirefields = 'thumbnails(select=c48x48,medium,large,c1500x1500)';

    /**
     * @var \TheLion\ShareoneDrive\App
     */
    private $_app;

    /**
     * @var \TheLion\ShareoneDrive\Processor
     */
    private $_processor;

    public function __construct(App $_app, Processor $_processor = null)
    {
        $this->_app = $_app;
        $this->_processor = $_processor;
    }

    /*
     * Get OneDrive information
     *
     * @return mixed|WP_Error
     */

    public function get_drive_info()
    {
        return $this->get_app()->get_drive()->about->get();
    }

    /*
     * Get Account infortmation
     *
     * @return mixed|WP_Error
     */

    public function get_account_info()
    {
        return $this->get_app()->get_user()->me->get();
    }

    public function get_root_folder()
    {
        $root_node = $this->get_cache()->get_root_node();

        if (false !== $root_node) {
            return $root_node;
        }

        $root_node = $this->get_cache()->get_root_node();

        if (false !== $root_node && null !== $root_node->get_entry()) {
            return $root_node;
        }

        // Top OneDrive Folder
        $root_api = new \SODOneDrive_Service_Drive_Item();
        $root_api->setId('drives');
        $root_api->setName('OneDrive');
        $root_api->setFolder(new \SODOneDrive_Service_Drive_FolderFacet());
        $root_entry = new Entry($root_api, true);
        $root_entry->set_virtual_folder('drives');
        $cached_root_node = $this->get_cache()->add_to_cache($root_entry);
        $cached_root_node->set_expired(null);
        $cached_root_node->set_root();
        $cached_root_node->set_loaded_children(true);
        $cached_root_node->set_virtual_folder('drives');
        $this->get_cache()->set_root_node_id('drives');

        // Sharepoint Drives
        $this->get_all_site_drives();

        // OneDrives
        $this->get_all_drives();

        $this->get_cache()->set_updated();

        return $this->get_cache()->get_root_node();
    }

    public function get_all_drives()
    {
        try {
            if ('personal' === $this->get_processor()->get_current_account()->get_type()) {
                $api_drive = $this->get_app()->get_drive()->items->root();

                $api_drive->setName(esc_html__('My files', 'wpcloudplugins'));
                $parent = $api_drive->getParentReference();
                $parent->setId('drives');
                $api_drive->setParentReference($parent);
                $drive_entry = new Entry($api_drive);
                $drive_entry->set_virtual_folder('drive');
                $cached_drive_node = $this->get_cache()->add_to_cache($drive_entry);
                $cached_drive_node->set_virtual_folder('drive');
            } else {
                $result = $this->get_app()->get_drive()->drives->list(['expand' => 'root']);

                $api_drives = $result->getValue();

                foreach ($api_drives as $api_drive) {
                    $root_folder = $api_drive->root;
                    $root_folder->setName($api_drive->getName());
                    $parent = $root_folder->getParentReference();
                    $parent->setId('drives');
                    $root_folder->setParentReference($parent);
                    $drive_entry = new Entry($root_folder);
                    $drive_entry->set_virtual_folder('drive');
                    $cached_drive_node = $this->get_cache()->add_to_cache($drive_entry);
                    $cached_drive_node->set_virtual_folder('drive');
                }
            }
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }
    }

    public function get_all_site_drives()
    {
        if ('personal' === $this->get_processor()->get_current_account()->get_type()) {
            return false;
        }

        try {
            $result = $this->get_app()->get_sites()->sites->search(['q' => '']);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }

        $api_sites = $result->getValue();

        if (0 === count($api_sites)) {
            return;
        }

        // Create Sites folder
        $sites_api = new \SODOneDrive_Service_Drive_Item();
        $sites_api->setId('sites');
        $sites_api->setName('SharePoint');
        $sites_api->setFolder(new \SODOneDrive_Service_Drive_FolderFacet());
        $parent = new \SODOneDrive_Service_Drive_ItemReference();
        $parent->setId('drives');
        $sites_api->setParentReference($parent);
        $site_entry = new Entry($sites_api, true);
        $site_entry->set_virtual_folder('sites');
        $cached_site_node = $this->get_cache()->add_to_cache($site_entry);
        $cached_site_node->set_expired(null);
        $cached_site_node->set_root();
        $cached_site_node->set_loaded_children(true);
        $cached_site_node->set_virtual_folder('sites');

        foreach ($api_sites as $api_site) {
            $api_site_info = $this->get_app()->get_sites()->sites->get($api_site->getId(), ['expand' => 'drives(expand=root)']);

            $site_drives = $api_site_info->getDrives();

            foreach ($site_drives as $api_drive) {
                $root_folder = $api_drive->root;
                $root_folder->setName($api_site->getDisplayName());
                $parent = $root_folder->getParentReference();
                $parent->setId('sites');
                $root_folder->setParentReference($parent);
                $drive_entry = new Entry($root_folder);
                $drive_entry->set_virtual_folder('drive');
                $cached_drive_node = $this->get_cache()->add_to_cache($drive_entry);
                $cached_drive_node->set_virtual_folder('drive');
            }
        }
    }

    /**
     * Get folders and files.
     *
     * @param string $folderid
     * @param bool   $checkauthorized
     * @param mixed  $drive_id
     *
     * @return array|bool
     */
    public function get_folder($folderid = false, $checkauthorized = true)
    {
        // Load the root folder when needed
        if ('root' !== $folderid) {
            $rootfolder = $this->get_root_folder();
        }

        if (false === $folderid) {
            $folderid = $this->get_processor()->get_requested_entry();
        }

        // Load cached folder if present
        $cachedfolder = $this->get_cache()->is_cached($folderid, 'id', false);

        // If folder isn't present in cache, load it
        if (!$cachedfolder) {
            // First find the Drive Id in cache before using the one provided in function argument

            $driveId = $this->get_cache()->get_drive_id_by_entry_id($folderid);
            $driveId = !empty($driveId) ? $driveId : $this->get_processor()->get_current_drive_id();

            try {
                $results = $this->get_app()->get_drive()->items->get($folderid, ['driveId' => $driveId, 'expand' => $this->apilistfilesfields]);
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return false;
            }

            $folder_entry = new Entry($results);
            $cachedfolder = $this->get_cache()->add_to_cache($folder_entry);

            try {
                $results_children = $this->get_app()->get_drive()->items->children($folderid, ['driveId' => $folder_entry->get_drive_id(), 'expand' => $this->apilistfilesfields]);
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return false;
            }

            $files_in_folder = $results_children->getValue();
            $next_page_token = $results_children['@odata.nextLink'];

            // Get all files in folder
            while (!empty($next_page_token)) {
                $next_link = parse_url($next_page_token);
                parse_str($next_link['query'], $next_link_attributes);
                $next_page_token = $next_link_attributes['$skiptoken'];

                try {
                    $more_files = $this->get_app()->get_drive()->items->children($folderid, ['driveId' => $folder_entry->get_drive_id(), 'expand' => $this->apilistfilesfields, 'skiptoken' => $next_page_token]);
                    $files_in_folder = array_merge($files_in_folder, $more_files->getValue());
                    $next_page_token = $more_files['@odata.nextLink'];
                } catch (\Exception $ex) {
                    error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                    return false;
                }
            }

            // Convert the items to Framework Entry

            $folder_items = [];

            // Add all entries in folder to cache

            foreach ($files_in_folder as $entry) {
                $item = new Entry($entry);
                $newitem = $this->get_cache()->add_to_cache($item);
            }

            $cachedfolder->set_loaded_children(true);
            $this->get_cache()->update_cache();
        }

        $folder = $cachedfolder;
        $files_in_folder = $cachedfolder->get_children();

        // Check if folder is in the shortcode-set rootfolder
        if (true === $checkauthorized) {
            if (!$this->get_processor()->_is_entry_authorized($cachedfolder)) {
                return false;
            }
        }

        return ['folder' => $folder, 'contents' => $files_in_folder];
    }

    // Get entry

    /**
     * @param type  $entryid
     * @param type  $checkauthorized
     * @param mixed $drive_id
     *
     * @return bool|\TheLion\ShareoneDrive\CacheNode
     */
    public function get_entry($entryid = false, $checkauthorized = true)
    {
        if (false === $entryid) {
            $entryid = $this->get_processor()->get_requested_entry();
        }

        // Load the root folder when needed
        $this->get_root_folder();

        // Get entry from cache
        $cachedentry = $this->get_cache()->is_cached($entryid);

        // Get metadata if entry isn't cached
        if (!$cachedentry) {
            $driveId = $this->get_cache()->get_drive_id_by_entry_id($entryid);
            $driveId = !empty($driveId) ? $driveId : $this->get_processor()->get_current_drive_id();

            try {
                $api_entry = $this->get_app()->get_drive()->items->get($entryid, ['driveId' => $driveId, 'expand' => $this->apifilefields]);
                $entry = new Entry($api_entry);
                $cachedentry = $this->get_cache()->add_to_cache($entry);
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return false;
            }
        }

        if (true === $checkauthorized) {
            if ('root' !== $entryid && !$this->get_processor()->_is_entry_authorized($cachedentry)) {
                return false;
            }
        }

        return $cachedentry;
    }

    /**
     * Get (and create) sub folder by path.
     *
     * @param string $parent_folder_id
     * @param string $subfolder_path
     * @param bool   $create_if_not_exists
     *
     * @return bool|\TheLion\ShareoneDrive\CacheNode
     */
    public function get_sub_folder_by_path($parent_folder_id, $subfolder_path, $create_if_not_exists = false)
    {
        $cached_parent_folder = $this->get_folder($parent_folder_id, false);

        if (empty($cached_parent_folder)) {
            return false;
        }

        if (empty($subfolder_path)) {
            return $cached_parent_folder['folder'];
        }

        $subfolders = array_filter(explode('/', $subfolder_path));
        $current_folder = array_shift($subfolders);

        //Try to load the subfolder at once
        $cached_sub_folder = $this->get_cache()->get_node_by_name($current_folder, $parent_folder_id);

        /* If folder isn't in cache yet,
          * Update the parent folder to make sure the latest version is loaded */
        if (false === $cached_sub_folder) {
            $this->get_cache()->pull_for_changes(true);
            $cached_sub_folder = $this->get_cache()->get_node_by_name($current_folder, $parent_folder_id);
        }

        if (false === $cached_sub_folder && false === $create_if_not_exists) {
            return false;
        }

        // If the subfolder can't be found, create the sub folder
        if (!$cached_sub_folder) {
            // Create new folder object
            $newfolder = new \SODOneDrive_Service_Drive_Item();
            $newfolder->setName($current_folder);
            $newfolder->setFolder(new \SODOneDrive_Service_Drive_FolderFacet());

            try {
                $api_entry = $this->get_app()->get_drive()->items->insert($parent_folder_id, $newfolder, ['driveId' => $cached_parent_folder['folder']->get_drive_id(), 'expand' => $this->apifilefields]);
                // Add new file to our Cache
                $newentry = new Entry($api_entry);
                $cached_sub_folder = $this->get_cache()->add_to_cache($newentry);

                do_action('shareonedrive_log_event', 'shareonedrive_created_entry', $cached_sub_folder);
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return false;
            }
        }

        return $this->get_sub_folder_by_path($cached_sub_folder->get_id(), implode('/', $subfolders), $create_if_not_exists);
    }

    public function update_expired_entry(CacheNode $cachedentry)
    {
        $entry = $cachedentry->get_entry();

        try {
            $api_entry = $this->get_app()->get_drive()->items->get($entry->get_id(), ['driveId' => $cachedentry->get_drive_id(), 'expand' => $this->apifilefieldsexpire]);
            $entry = new Entry($api_entry);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }

        return $this->get_cache()->add_to_cache($entry);
    }

    public function update_expired_folder(CacheNode $cachedentry)
    {
        $entry = $cachedentry->get_entry();

        try {
            $results_children = $this->get_app()->get_drive()->items->children($entry->get_id(), ['driveId' => $cachedentry->get_drive_id(), 'expand' => $this->apilistfilesexpirefields]);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }

        $files_in_folder = $results_children->getValue();
        $next_page_token = $results_children['@odata.nextLink'];

        // Get all files in folder
        while (!empty($next_page_token)) {
            $next_link = parse_url($next_page_token);
            parse_str($next_link['query'], $next_link_attributes);
            $next_page_token = $next_link_attributes['$skiptoken'];

            try {
                $more_files = $this->get_app()->get_drive()->items->children($entry->get_id(), ['driveId' => $cachedentry->get_drive_id(), 'expand' => $this->apilistfilesfields, 'skiptoken' => $next_page_token]);
                $files_in_folder = array_merge($files_in_folder, $more_files->getValue());
                $next_page_token = isset($next_link_attributes['@odata.nextLink']) ? $next_link_attributes['@odata.nextLink'] : null;
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return false;
            }
        }

        $folder_items = [];
        $current_children = $cachedentry->get_children();
        foreach ($files_in_folder as $api_entry) {
            $entry = new Entry($api_entry);
            $this->get_cache()->add_to_cache($entry);
        }

        $this->get_cache()->add_to_cache($cachedentry->get_entry());

        return $cachedentry;
    }

    // Search entry by name

    public function search_by_name($query)
    {
        if ('parent' === $this->get_processor()->get_shortcode_option('searchfrom')) {
            $searched_folder_id = $this->get_processor()->get_requested_entry();
        } else {
            $searched_folder_id = $this->get_root_folder()->get_id();
        }

        $searched_folder = $this->get_folder($searched_folder_id, true);
        // Find all items containing query
        $params = ['id' => $searched_folder_id, 'driveId' => $searched_folder['folder']->get_drive_id(), 'q' => stripslashes($query), 'expand' => $this->apilistfilesfields];

        // Do the request
        $next_page_token = null;
        $files_found = [];
        $entries_found = [];
        $entries_in_searchedfolder = [];

        do_action('shareonedrive_log_event', 'shareonedrive_searched', $searched_folder['folder'], ['query' => $query]);

        do {
            try {
                $search_response = $this->get_app()->get_drive()->items->search($params);
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return [];
            }

            // Process the response
            $more_files = $search_response->getValue();
            $files_found = array_merge($files_found, $more_files);

            if (isset($search_response['@odata.nextLink'])) {
                $next_page_token = $search_response['@odata.nextLink'];
                $next_link = parse_url($next_page_token);
                parse_str($next_link['query'], $next_link_attributes);
                $next_page_token = isset($next_link_attributes['$skiptoken']) ? $next_link_attributes['$skiptoken'] : null;
            } else {
                $next_page_token = null;
            }
            $params['skiptoken'] = $next_page_token;
        } while (null !== $next_page_token);

        foreach ($files_found as $file) {
            $entries_found[] = new Entry($file);
        }

        foreach ($entries_found as $entry) {
            // Check if entries are in cache
            $cachedentry = $this->get_cache()->is_cached($entry->get_id());

            // If not found, add to cache
            if (false === $cachedentry) {
                $cachedentry = $this->get_cache()->add_to_cache($entry);
            }

            $parent_folder = ('parent' === $this->get_processor()->get_shortcode_option('searchfrom')) ? $this->get_processor()->get_requested_entry() : $this->get_root_folder()->get_id();

            // Update the time_limit as this can take a while
            @set_time_limit(30);

            if (false === $cachedentry->is_in_folder($parent_folder)) {
                continue;
            }

            if (false === $this->get_processor()->_is_entry_authorized($cachedentry)) {
                continue;
            }

            $entries_in_searchedfolder[] = $cachedentry;
        }

        // Update the cache already here so that the Search Output is cached
        $this->get_cache()->update_cache();

        return ['folder' => $searched_folder['folder'], 'contents' => $entries_in_searchedfolder];
    }

    // Delete multiple entries from OneDrive

    public function delete_entries($entries_to_delete = [])
    {
        $deleted_entries = [];

        foreach ($entries_to_delete as $target_entry_path) {
            $target_cached_entry = $this->get_entry($target_entry_path);

            if (false === $target_cached_entry) {
                continue;
            }

            $target_entry = $target_cached_entry->get_entry();

            if ($target_entry->is_file() && false === $this->get_processor()->get_user()->can_delete_files()) {
                error_log('[WP Cloud Plugin message]: '.sprintf('Failed to delete %s as user is not allowed to remove files.', $target_entry->get_path()));

                continue;
            }

            if ($target_entry->is_dir() && false === $this->get_processor()->get_user()->can_delete_folders()) {
                error_log('[WP Cloud Plugin message]: '.sprintf('Failed to delete %s as user is not allowed to remove folders.', $target_entry->get_path()));

                continue;
            }

            if ('1' === $this->get_processor()->get_shortcode_option('demo')) {
                continue;
            }

            try {
                /* Issue with if-match header
                 * https://github.com/OneDrive/onedrive-api-docs/issues/131
                 * If solved, change to:
                 * $headers = array("if-match" => '*'); */
                $params = ['driveId' => $target_cached_entry->get_drive_id()];
                $deleted_entry = $this->get_app()->get_drive()->items->delete($target_entry->get_id(), $params);
                $deleted_entries[$target_entry->get_id()] = $target_cached_entry;

                do_action('shareonedrive_log_event', 'shareonedrive_deleted_entry', $target_cached_entry, []);

                $this->get_cache()->remove_from_cache($target_entry->get_id(), 'deleted');
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));
            }
        }

        if ('1' === $this->get_processor()->get_shortcode_option('notificationdeletion')) {
            // TO DO NOTIFICATION
            $this->get_processor()->send_notification_email('deletion', $deleted_entries);
        }

        // Remove items from cache
        $this->get_cache()->pull_for_changes(true);

        // Clear Cached Requests
        CacheRequest::clear_request_cache();

        return $deleted_entries;
    }

    // Rename entry from OneDrive

    public function rename_entry($new_filename = null)
    {
        if ('1' === $this->get_processor()->get_shortcode_option('demo')) {
            return new \WP_Error('broke', esc_html__('Failed to rename entry', 'wpcloudplugins'));
        }

        if (null === $new_filename && '1' === $this->get_processor()->get_shortcode_option('debug')) {
            return new \WP_Error('broke', esc_html__('No new name set', 'wpcloudplugins'));
        }

        // Get entry meta data
        $cachedentry = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if (false === $cachedentry) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
            if (false === $cachedentry) {
                if ('1' === $this->get_processor()->get_shortcode_option('debug')) {
                    return new \WP_Error('broke', esc_html__('Invalid entry', 'wpcloudplugins'));
                }

                return new \WP_Error('broke', esc_html__('Failed to rename entry', 'wpcloudplugins'));

                return new \WP_Error('broke', esc_html__('Failed to rename entry', 'wpcloudplugins'));
            }
        }

        // Check if user is allowed to delete from this dir
        if (!$cachedentry->is_in_folder($this->get_processor()->get_last_folder())) {
            return new \WP_Error('broke', esc_html__('You are not authorized to rename files in this directory', 'wpcloudplugins'));
        }

        $entry = $cachedentry->get_entry();

        // Check user permission
        if (!$entry->get_permission('canrename')) {
            return new \WP_Error('broke', esc_html__('You are not authorized to rename this file or folder', 'wpcloudplugins'));
        }

        // Check if entry is allowed
        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            return new \WP_Error('broke', esc_html__('You are not authorized to rename this file or folder', 'wpcloudplugins'));
        }

        if ($entry->is_dir() && (false === $this->get_processor()->get_user()->can_rename_folders())) {
            return new \WP_Error('broke', esc_html__('You are not authorized to rename folder', 'wpcloudplugins'));
        }

        if ($entry->is_file() && (false === $this->get_processor()->get_user()->can_rename_files())) {
            return new \WP_Error('broke', esc_html__('You are not authorized to rename this file', 'wpcloudplugins'));
        }

        $extension = $entry->get_extension();
        $name = (!empty($extension)) ? $new_filename.'.'.$extension : $new_filename;
        $updaterequest = ['name' => $name];

        try {
            $renamed_entry = $this->update_entry($entry->get_id(), $updaterequest, ['driveId' => $cachedentry->get_drive_id()]);

            if (false !== $renamed_entry && null !== $renamed_entry) {
                $this->get_cache()->update_cache();
            }

            do_action('shareonedrive_log_event', 'shareonedrive_renamed_entry', $renamed_entry, ['old_name' => $entry->get_name()]);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ('1' === $this->get_processor()->get_shortcode_option('debug')) {
                return new \WP_Error('broke', $ex->getMessage());
            }

            return new \WP_Error('broke', esc_html__('Failed to rename entry', 'wpcloudplugins'));
        }

        $this->get_cache()->pull_for_changes(true);

        return $renamed_entry;
    }

    // Copy entry

    public function copy_entry($cached_entry = null, $cached_parent = null, $new_name = null)
    {
        if (null === $cached_entry) {
            $cached_entry = $this->get_entry($this->get_processor()->get_requested_entry());
        }

        if (null === $cached_parent) {
            $cached_parent = $this->get_entry($this->get_processor()->get_last_folder());
        }

        if (false === $cached_entry) {
            $message = '[WP Cloud Plugin message]: Failed to copy the file.';

            error_log($message);

            return new \WP_Error('broke', $message);
        }

        $entry = $cached_entry->get_entry();

        if (($entry->is_dir()) && (false === $this->get_processor()->get_user()->can_copy_folders())) {
            $message = '[WP Cloud Plugin message]: '.sprintf('Failed to move %s as user is not allowed to move folders.', $cached_parent->get_path($this->get_processor()->get_root_folder()));

            error_log($message);

            return new \WP_Error('broke', $message);
        }

        if (($entry->is_file()) && (false === $this->get_processor()->get_user()->can_copy_files())) {
            $message = '[WP Cloud Plugin message]: '.sprintf('Failed to copy %s as user is not allowed to copy files.', $cached_parent->get_path($this->get_processor()->get_root_folder()));

            error_log($message);

            return new \WP_Error('broke', $message);
        }

        if ('1' === $this->get_processor()->get_shortcode_option('demo')) {
            $message = '[WP Cloud Plugin message]: '.sprintf('Failed to copy the file %s.', $cached_entry->get_path($this->get_processor()->get_root_folder()));

            error_log($message);

            return new \WP_Error('broke', $message);
        }

        // Check if user is allowed to copy from this dir
        if (!$cached_entry->is_in_folder($cached_parent->get_id())) {
            $message = '[WP Cloud Plugin message]: '.sprintf('Failed to copy %s as user is not allowed to copy items in this directory.', $cached_parent->get_path($this->get_processor()->get_root_folder()));

            error_log($message);

            return new \WP_Error('broke', $message);
        }

        // Create an the entry for Patch
        $new_parent = new \SODOneDrive_Service_Drive_ItemReference();
        $new_parent->setId($cached_parent->get_id());
        $updaterequest = new \SODOneDrive_Service_Drive_Item();
        $updaterequest->setParentReference($new_parent);

        $extension = $entry->get_extension();
        $name = (!empty($extension)) ? $new_name.'.'.$extension : $new_name;
        $updaterequest->setName($name);
        $updaterequest['@microsoft.graph.conflictBehavior'] = 'rename';

        try {
            $result = $this->get_app()->get_drive()->items->copy($cached_entry->get_id(), $updaterequest, ['driveId' => $cached_entry->get_drive_id()]);
            usleep(1000000); // API call is async.

            do_action('shareonedrive_log_event', 'shareonedrive_copied_entry', $cached_entry, ['original' => $entry->get_name()]);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ('1' === $this->get_processor()->get_shortcode_option('debug')) {
                return new \WP_Error('broke', $ex->getMessage());
            }

            return new \WP_Error('broke', esc_html__('Failed to copy entry', 'wpcloudplugins'));
        }

        // Clear Cached Requests
        CacheRequest::clear_local_cache_for_shortcode($this->get_processor()->get_listtoken());

        return true;
    }

    // Edit descriptions entry from OneDrive
    public function move_entries($entries, $target, $copy = false)
    {
        $entries_to_move = [];

        $cached_target = $this->get_entry($target);
        $cached_current_folder = $this->get_entry($this->get_processor()->get_last_folder());

        if (false === $cached_target) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Failed to move as target folder %s is not found.', $target));

            return $entries_to_move;
        }

        // Set new parent
        $new_parent = new \SODOneDrive_Service_Drive_ItemReference();
        $new_parent->setId($cached_target->get_id());
        $new_parent->setDriveId($cached_target->get_drive_id());
        $updaterequest = new \SODOneDrive_Service_Drive_Item();
        $updaterequest->setParentReference($new_parent);
        $updaterequest['@microsoft.graph.conflictBehavior'] = 'rename';

        foreach ($entries as $entry_id) {
            $cached_entry = $this->get_entry($entry_id);

            if (false === $cached_entry) {
                continue;
            }

            $entry = $cached_entry->get_entry();

            if (($entry->is_dir()) && (false === $this->get_processor()->get_user()->can_move_folders())) {
                error_log('[WP Cloud Plugin message]: '.sprintf('Failed to move %s as user is not allowed to move folders.', $cached_target->get_name()));
                $entries_to_move[$cached_entry->get_id()] = false;

                continue;
            }

            if (($entry->is_file()) && (false === $this->get_processor()->get_user()->can_move_files())) {
                error_log('[WP Cloud Plugin message]: '.sprintf('Failed to move %s as user is not allowed to remove files.', $cached_target->get_name()));
                $entries_to_move[$cached_entry->get_id()] = false;

                continue;
            }

            if ('1' === $this->get_processor()->get_shortcode_option('demo')) {
                $entries_to_move[$cached_entry->get_id()] = false;

                continue;
            }

            // Check if user is allowed to delete from this dir
            if (!$cached_entry->is_in_folder($cached_current_folder->get_id())) {
                error_log('[WP Cloud Plugin message]: '.sprintf('Failed to move %s as user is not allowed to move items in this directory.', $cached_target->get_name()));
                $entries_to_move[$cached_entry->get_id()] = false;

                continue;
            }

            // Check user permission
            if (!$entry->get_permission('canmove')) {
                error_log('[WP Cloud Plugin message]: '.sprintf('Failed to move %s as the sharing permissions on it prevent this.', $cached_target->get_name()));
                $entries_to_move[$cached_entry->get_id()] = false;

                continue;
            }

            try {
                if ($copy) {
                    $this->get_app()->get_drive()->items->copy($cached_entry->get_id(), $updaterequest, ['driveId' => $cached_entry->get_drive_id(), 'prefer' => 'respond-async']);
                    // Copying can take some time, so the target folder is removed from the cache and will be loaded if the user enters that directory
                    $cached_target->set_loaded(false);
                    $updated_entry = $cached_entry;
                } else {
                    $updated_entry = $this->update_entry($entry->get_id(), $updaterequest, ['driveId' => $cached_entry->get_drive_id(), 'prefer' => 'respond-async']);
                }

                do_action('shareonedrive_log_event', 'shareonedrive_moved_entry', $updated_entry);

                $entries_to_move[$cached_entry->get_id()] = $updated_entry;
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));
                $entries_to_move[$cached_entry->get_id()] = false;

                continue;
            }
        }

        // Clear Cached Requests
        CacheRequest::clear_local_cache_for_shortcode($this->get_processor()->get_listtoken());

        return $entries_to_move;
    }

    // Edit description of entry

    public function update_description($new_description = null)
    {
        if (null === $new_description && '1' === $this->get_processor()->get_shortcode_option('debug')) {
            return new \WP_Error('broke', esc_html__('No new description set', 'wpcloudplugins'));
        }

        // Get entry meta data
        $cachedentry = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if (false === $cachedentry) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
            if (false === $cachedentry) {
                if ('1' === $this->get_processor()->get_shortcode_option('debug')) {
                    return new \WP_Error('broke', esc_html__('Invalid entry', 'wpcloudplugins'));
                }

                return new \WP_Error('broke', esc_html__('Failed to edit entry', 'wpcloudplugins'));

                return new \WP_Error('broke', esc_html__('Failed to edit entry', 'wpcloudplugins'));
            }
        }

        // Check if user is allowed to delete from this dir
        if (!$cachedentry->is_in_folder($this->get_processor()->get_last_folder())) {
            return new \WP_Error('broke', esc_html__('You are not authorized to edit files in this directory', 'wpcloudplugins'));
        }

        $entry = $cachedentry->get_entry();

        // Check if entry is allowed
        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            return new \WP_Error('broke', esc_html__('You are not authorized to edit this file or folder', 'wpcloudplugins'));
        }

        // Set new description, and update the entry
        $updaterequest = ['description' => $new_description];

        try {
            $edited_entry = $this->update_entry($entry->get_id(), $updaterequest, ['driveId' => $cachedentry->get_drive_id()]);

            if (false !== $edited_entry && null !== $edited_entry) {
                do_action('shareonedrive_log_event', 'shareonedrive_updated_metadata', $edited_entry, ['metadata_field' => 'Description']);
                $this->get_cache()->update_cache();
            }
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ('1' === $this->get_processor()->get_shortcode_option('debug')) {
                return new \WP_Error('broke', $ex->getMessage());
            }

            return new \WP_Error('broke', esc_html__('Failed to edit entry', 'wpcloudplugins'));
        }

        return $edited_entry->get_entry()->get_description();
    }

    // Update entry from OneDrive

    public function update_entry($entry_id, $updaterequest = [], $params = [])
    {
        $params = array_merge(['expand' => $this->apilistfilesfields], $params);

        try {
            $api_entry = $this->get_app()->get_drive()->items->patch($entry_id, $updaterequest, $params);
            $entry = new Entry($api_entry);

            // Remove item from cache if it is moved
            if ($updaterequest instanceof \SODOneDrive_Service_Drive_Item && null !== $updaterequest->getParentReference()) {
                $this->get_cache()->remove_from_cache($entry_id, 'deleted');
            }

            $cachedentry = $this->get_cache()->add_to_cache($entry);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            throw $ex;
        }

        return $cachedentry;
    }

    // Add directory to OneDrive

    public function add_folder($new_folder_name = null)
    {
        if ('1' === $this->get_processor()->get_shortcode_option('demo')) {
            return new \WP_Error('broke', esc_html__('Failed to add folder', 'wpcloudplugins'));
        }

        if (null === $new_folder_name && '1' === $this->get_processor()->get_shortcode_option('debug')) {
            return new \WP_Error('broke', esc_html__('No new foldername set', 'wpcloudplugins'));
        }

        // Get entry meta data of current folder
        $cachedentry = $this->get_cache()->is_cached($this->get_processor()->get_last_folder());

        if (false === $cachedentry) {
            $cachedentry = $this->get_entry($this->get_processor()->get_last_folder());
            if (false === $cachedentry) {
                if ('1' === $this->get_processor()->get_shortcode_option('debug')) {
                    return new \WP_Error('broke', esc_html__('Invalid entry', 'wpcloudplugins'));
                }

                return new \WP_Error('broke', esc_html__('Failed to add entry', 'wpcloudplugins'));

                return new \WP_Error('broke', esc_html__('Failed to add entry', 'wpcloudplugins'));
            }
        }

        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            return new \WP_Error('broke', esc_html__('You are not authorized to add folders in this directory', 'wpcloudplugins'));
        }

        $currentfolder = $cachedentry->get_entry();

        // Check user permission
        if (!$currentfolder->get_permission('canadd')) {
            return new \WP_Error('broke', esc_html__('You are not authorized to add a folder', 'wpcloudplugins'));
        }

        // Create new folder object
        $newfolder = new \SODOneDrive_Service_Drive_Item();
        $newfolder->setName($new_folder_name);
        $newfolder->setFolder(new \SODOneDrive_Service_Drive_FolderFacet());
        $newfolder['@microsoft.graph.conflictBehavior'] = 'rename';

        try {
            $api_entry = $this->get_app()->get_drive()->items->insert($currentfolder->get_id(), $newfolder, ['driveId' => $cachedentry->get_drive_id(), 'expand' => $this->apifilefields]);
            // Add new file to our Cache
            $newentry = new Entry($api_entry);
            $cached_entry = $this->get_cache()->add_to_cache($newentry);

            do_action('shareonedrive_log_event', 'shareonedrive_created_entry', $cached_entry);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ('1' === $this->get_processor()->get_shortcode_option('debug')) {
                return new \WP_Error('broke', $ex->getMessage());
            }

            return new \WP_Error('broke', esc_html__('Failed to add folder', 'wpcloudplugins'));
        }

        // Remove items from cache
        $this->get_cache()->pull_for_changes(true);

        return $cached_entry;
    }

    public function get_folder_thumbnails($folders_ids = [], $height, $width, $maximages = 3)
    {
        $thumbnails = [];

        foreach ($folders_ids as $folder_id) {
            $folder_data = $this->get_folder($folder_id);
            $cached_folder_node = $folder_data['folder'];

            if (false === $cached_folder_node->has_children()) {
                continue;
            }

            $children = $folder_data['contents'];
            // Try to load default thumbnail (.thumb.jpg or .thumb.png){
            $default_thumb = $this->get_cache()->get_node_by_name('.thumb.jpg', $cached_folder_node);
            if (empty($default_thumb)) {
                $default_thumb = $this->get_cache()->get_node_by_name('.thumb.png', $cached_folder_node);
            }

            if (!empty($default_thumb)) {
                $children = [$default_thumb];
            }

            $thumbnails[$folder_id] = $folder_thumbs = [];

            // Else get $maximages images from the folder
            for ($i = 1; $i <= $maximages; ++$i) {
                $cached_child_node = current($children);
                $child_entry = $cached_child_node->get_entry();

                // Skip Folder children or files without thumbnail
                if ($cached_child_node->is_dir() || !$child_entry->has_own_thumbnail()) {
                    --$i;

                    if (false === next($children)) {
                        break;
                    }

                    continue;
                }

                //Get Thumbnail from child
                $thumbnail_url = $child_entry->get_thumbnail_with_size($height, $width, 'center');

                //Set an array of thumbnails for the requested folder ID
                $thumbnails[$folder_id][] = $thumbnail_url;

                //Set a new Thumbnailset for the folder
                $medium_thumbnail = new \SODOneDrive_Service_Drive_Thumbnail();
                $medium_thumbnail->setUrl($thumbnail_url);
                $thumbnail_set = new \SODOneDrive_Service_Drive_ThumbnailSet();
                $thumbnail_set->setC48x48($medium_thumbnail);
                $thumbnail_set->setMedium($medium_thumbnail);
                $thumbnail_set->setSmall($medium_thumbnail);

                $folder_thumbs[] = $thumbnail_set;
                // Stop if there are no further children
                if (false === next($children)) {
                    break;
                }
            }

            $cached_folder_node->get_entry()->set_folder_thumbnails($folder_thumbs);
            $cached_folder_node->get_entry()->set_has_own_thumbnail(true);
        }

        // Clear Cache
        $this->get_cache()->set_updated();

        return $thumbnails;
    }

    public function preview_entry()
    {
        // Get file meta data
        $cached_entry = $this->get_entry();

        if (false === $cached_entry) {
            exit('-1');
        }

        $entry = $cached_entry->get_entry();
        if (false === $entry->get_can_preview_by_cloud()) {
            exit('-1');
        }

        if (false === $this->get_processor()->get_user()->can_preview()) {
            exit('-1');
        }

        do_action('shareonedrive_log_event', 'shareonedrive_previewed_entry', $cached_entry);

        // Preview for Image files
        if (in_array($entry->get_extension(), ['jpg', 'jpeg', 'gif', 'png'])) {
            if (isset($_REQUEST['inline']) && $this->get_processor()->get_user()->can_download()) {
                $url = $this->get_shared_link($cached_entry).'?raw=1';
                header('Location: '.$url);

                exit();
            }

            if ('onedrivethumbnail' === $this->get_processor()->get_setting('loadimages') || false === $this->get_processor()->get_user()->can_download()) {
                if (null !== $entry->get_thumbnail_original()) {
                    header('Location: '.$entry->get_thumbnail_original());

                    exit();
                }
                if (null !== $entry->get_thumbnail_large()) {
                    header('Location: '.$entry->get_thumbnail_large());

                    exit();
                }
            }
        }

        // Preview for Media files in HTML5 Player + PDF files
        if (in_array($entry->get_extension(), ['jpg', 'jpeg', 'gif', 'png', 'mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'mp3', 'm4a', 'ogg', 'oga'])) {
            $temporarily_link = $this->get_temporarily_link($cached_entry);
            header('Location: '.$temporarily_link);

            exit();
        }

        // Business accounts can use the Preview API */
        if ('personal' !== $this->get_processor()->get_current_account()->get_type()) {
            $preview_link = $this->get_preview_link($cached_entry);
            header('Location: '.$preview_link);

            exit();
        }

        // Personal accounts need to use Shared/Temporarily links
        if (in_array($entry->get_extension(), ['pdf'])) {
            $temporarily_link = $this->get_embedded_link($cached_entry);
            header('Location: '.$temporarily_link);

            exit();
        }

        // Preview for Office files
        if (in_array($entry->get_extension(), ['csv', 'doc', 'docx', 'odp', 'ods', 'odt', 'pot', 'potm', 'potx', 'pps', 'ppsx', 'ppsxm', 'ppt', 'pptm', 'pptx', 'rtf', 'xls', 'xlsx'])) {
            if ($this->get_processor()->get_user()->can_edit()) {
                // If user has permissions to edit the file, show the preview in Office Online
                $shared_link = $this->get_embedded_link($cached_entry);

                if (!empty($shared_link)) {
                    header('Location: '.$shared_link);

                    exit();
                }
            }
        }

        // Preview for all other formats
        $temporarily_link = $this->get_temporarily_link($cached_entry, 'pdf');

        header('Location: '.$temporarily_link);

        exit();
    }

    public function edit_entry()
    {
        // Get file meta data
        $cached_entry = $this->get_entry();

        if (false === $cached_entry) {
            exit('-1');
        }

        $entry = $cached_entry->get_entry();
        if (false === $entry->get_can_edit_by_cloud()) {
            exit('-1');
        }

        $edit_link = $this->get_shared_link($cached_entry, 'edit');

        if (empty($edit_link)) {
            error_log('[WP Cloud Plugin message]: '.sprintf('Cannot create a editable shared link %s', __LINE__));

            exit();
        }

        do_action('shareonedrive_edit', $cached_entry);
        do_action('shareonedrive_log_event', 'shareonedrive_edited_entry', $cached_entry);

        header('Location: '.$edit_link);

        exit();
    }

    // Download file

    public function download_entry()
    {
        // Check if file is cached and still valid
        $cached = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if (false === $cached) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
        } else {
            $cachedentry = $cached;
        }

        if (false === $cachedentry) {
            exit();
        }

        $entry = $cachedentry->get_entry();

        // get the last-modified-date of this very file
        $lastModified = $entry->get_last_edited();
        // get a unique hash of this file (etag)
        $etagFile = md5($lastModified);
        // get the HTTP_IF_MODIFIED_SINCE header if set
        $ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
        // get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
        $etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

        header('Last-Modified: '.gmdate('D, d M Y H:i:s', $lastModified).' GMT');
        header("Etag: {$etagFile}");
        header('Expires: '.gmdate('D, d M Y H:i:s', time() + 60 * 5).' GMT');
        header('Cache-Control: must-revalidate');

        // check if page has changed. If not, send 304 and exit
        if (false !== $cached) {
            if (false !== $lastModified && (@strtotime($ifModifiedSince) == $lastModified || $etagHeader == $etagFile)) {
                // Send email if needed
                if ('1' === $this->get_processor()->get_shortcode_option('notificationdownload')) {
                    $this->get_processor()->send_notification_email('download', [$cachedentry]);
                }

                do_action('shareonedrive_download', $cachedentry);
                header('HTTP/1.1 304 Not Modified');

                exit;
            }
        }

        // Check if entry is allowed
        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            exit();
        }

        // Send email if needed
        if ('1' === $this->get_processor()->get_shortcode_option('notificationdownload')) {
            $this->get_processor()->send_notification_email('download', [$cachedentry]);
        }

        // Redirect if needed
        if ('url' === $entry->get_extension()) {
            $download_url = $this->get_temporarily_link($cachedentry, 'default');
            $request = new \SODOneDrive_Http_Request($download_url, 'GET');

            try {
                $httpRequest = $this->get_app()->get_client()->getAuth()->authenticatedRequest($request);
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                exit();
            }

            preg_match_all('/URL=(.*)/', $httpRequest->getResponseBody(), $location, PREG_SET_ORDER);

            if (2 === count($location[0])) {
                $temporarily_link = $location[0][1];
                header('Location: '.$temporarily_link);

                exit();
            }
        }

        if (isset($_REQUEST['redirect']) && ('redirect' === $this->get_processor()->get_shortcode_option('onclick'))) {
            $shared_link = $this->get_shared_link($cachedentry, 'view');
            header('Location: '.$shared_link);

            exit();
        }

        // Get the complete file
        $extension = (isset($_REQUEST['extension'])) ? $_REQUEST['extension'] : 'default';
        $this->download_content($cachedentry, $extension);

        exit();
    }

    public function download_content(CacheNode $cachedentry, $extension = 'default')
    {
        // If there is a temporarily download url present for this file, just redirect the user
        $stream = (isset($_REQUEST['action']) && 'shareonedrive-stream' === $_REQUEST['action'] && !isset($_REQUEST['caption']));
        $stored_url = ($stream) ? get_transient('shareonedrive'.$cachedentry->get_id().'_'.$cachedentry->get_entry()->get_extension()) : get_transient('shareonedrive'.$cachedentry->get_id().'_'.$cachedentry->get_entry()->get_extension());
        if (false !== $stored_url && filter_var($stored_url, FILTER_VALIDATE_URL)) {
            do_action('shareonedrive_download', $cachedentry, $stored_url);
            header('Location: '.$stored_url);

            exit();
        }

        $temporarily_link = $this->get_temporarily_link($cachedentry, $extension);

        // Download Hook
        do_action('shareonedrive_download', $cachedentry, $temporarily_link);

        $event_type = ($stream) ? 'shareonedrive_streamed_entry' : 'shareonedrive_downloaded_entry';
        do_action('shareonedrive_log_event', $event_type, $cachedentry);

        header('Location: '.$temporarily_link);

        set_transient('shareonedrive'.(($stream) ? 'stream' : 'download').'_'.$cachedentry->get_id().'_'.$cachedentry->get_entry()->get_extension(), $temporarily_link, MINUTE_IN_SECONDS * 10);

        exit();
    }

    public function download_via_proxy(Entry $entry, $url)
    {
        if (ob_get_level() > 0) {
            ob_end_clean(); // Stop WP from buffering
        }

        set_time_limit(500);

        $filename = basename($entry->get_name());

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; '.sprintf('filename="%s"; ', rawurlencode($filename)).sprintf("filename*=utf-8''%s", rawurlencode($filename)));
        header("Content-length: {$entry->get_size()}");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch, CURLOPT_WRITEFUNCTION, function ($curl, $data) {
            echo $data;

            return strlen($data);
        });
        curl_exec($ch);
        curl_close($ch);

        exit();
    }

    public function stream_entry()
    {
        // Check if file is cached and still valid
        $cached = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if (false === $cached) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
        } else {
            $cachedentry = $cached;
        }

        if (false === $cachedentry) {
            exit();
        }

        $entry = $cachedentry->get_entry();

        $extension = $entry->get_extension();
        $allowedextensions = ['mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'mp3', 'm4a', 'oga', 'wav', 'webm', 'vtt'];

        if (empty($extension) || !in_array($extension, $allowedextensions)) {
            exit();
        }

        // Download Captions directly
        if ('vtt' === $extension) {
            $temporarily_link = $this->get_temporarily_link($cachedentry, 'default');
            $this->download_via_proxy($entry, $temporarily_link);

            exit();
        }

        $this->download_entry();
    }

    public function get_folder_recursive(CacheNode $cached_entry, $list_of_cached_entries = [])
    {
        if (false === $this->get_processor()->_is_entry_authorized($cached_entry)) {
            return $list_of_cached_entries;
        }

        if ($cached_entry->get_entry()->is_file()) {
            $list_of_cached_entries[$cached_entry->get_id()] = $cached_entry;

            return $list_of_cached_entries;
        }

        $result = $this->get_folder($cached_entry->get_id());
        if (empty($result)) {
            return $list_of_cached_entries;
        }

        $cached_folder = $result['folder'];

        if (false === $cached_folder->has_children()) {
            return $list_of_cached_entries;
        }

        foreach ($cached_folder->get_children() as $cached_child_entry) {
            $new_of_cached_entries = $this->get_folder_recursive($cached_child_entry, $list_of_cached_entries);
            $list_of_cached_entries = array_merge($list_of_cached_entries, $new_of_cached_entries);
        }

        return $list_of_cached_entries;
    }

    /**
     * PREVIEW API ONLY WORKING FOR BUSINESS ACCOUNTS.
     *
     * @param string $mode
     * @param mixed  $params
     */
    public function has_preview_link(CacheNode $cached_entry, $params)
    {
        return get_transient('shareonedrive_'.$cached_entry->get_id().'_preview_'.implode('_', array_values($params)));

        return false;
    }

    /*
     * PREVIEW API ONLY WORKING FOR BUSINESS ACCOUNTS.

     * @param CacheNode $cached_entry
     * @param mixed $mode
     */
    public function get_preview_link(CacheNode $cached_entry, $params = [])
    {
        if ($preview_url = $this->has_preview_link($cached_entry, $params)) {
            return $preview_url;
        }

        try {
            $preview_url = $this->get_app()->get_drive()->items->preview($cached_entry->get_id(), $params, ['driveId' => $cached_entry->get_drive_id()]);
            set_transient('shareonedrive_'.$cached_entry->get_id().'_preview_'.implode('_', array_values($params)), $preview_url->getUrl(), MINUTE_IN_SECONDS * 10);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }

        return $preview_url->getUrl();
    }

    public function has_temporarily_link($cached_entry, $extension = 'default')
    {
        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if (false !== $cached_entry) {
            if ($temporarily_link = $cached_entry->get_temporarily_link($extension)) {
                return true;
            }
        }

        return false;
    }

    public function get_temporarily_link($cached_entry, $extension = 'default')
    {
        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        // 1: Get Temporarily link from cache
        if (false !== $cached_entry) {
            if ($temporarily_link = $cached_entry->get_temporarily_link($extension)) {
                return $temporarily_link;
            }
        }

        // 2: Get Temporarily link from entry itself
        /* $direct_download_link = $cached_entry->get_entry()->get_direct_download_link();
          if (!empty($direct_download_link) && $extension === 'default') {
          $cached_entry->add_temporarily_link($direct_download_link, $extension);
          $this->get_cache()->set_updated();
          return $cached_entry->get_temporarily_link($extension);
          } */

        // 3: Get Temporarily link via API
        try {
            // Get a Download link via the Graph API
            if ('default' === $extension) {
                $url = $this->get_app()->get_drive()->items->download($cached_entry->get_id(), ['driveId' => $cached_entry->get_drive_id()]);
            } else {
                $url = $this->get_app()->get_drive()->items->export($cached_entry->get_id(), ['driveId' => $cached_entry->get_drive_id(), 'format' => $extension]);
            }

            if (!empty($url)) {
                $cached_entry->add_temporarily_link($url, $extension);
            } else {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s', __LINE__));

                return false;
            }
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return false;
        }

        $this->get_cache()->set_updated();

        return $cached_entry->get_temporarily_link($extension);
    }

    public function has_shared_link($cached_entry, $mode = 'view', $scope = false)
    {
        if (false === $scope) {
            $scope = ('personal' === $this->get_processor()->get_current_account()->get_type()) ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if (false !== $cached_entry) {
            if ($shared_link = $cached_entry->get_shared_link($mode, $scope)) {
                return true;
            }
        }

        return false;
    }

    public function get_shared_link($cached_entry, $type = 'view', $scope = false)
    {
        if (false === $scope) {
            $scope = ('personal' === $this->get_processor()->get_current_account()->get_type()) ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if (false !== $cached_entry) {
            if ($shared_link = $cached_entry->get_shared_link($type, $scope)) {
                do_action('shareonedrive_log_event', 'shareonedrive_created_link_to_entry', $cached_entry, ['url' => $shared_link]);

                return $shared_link;
            }
        }

        $shared_link = $this->create_shared_link($cached_entry, $type, $scope);
        do_action('shareonedrive_log_event', 'shareonedrive_created_link_to_entry', $cached_entry, ['url' => $shared_link]);

        return $shared_link;
    }

    public function get_shared_link_for_output($entry_id = false)
    {
        $cached_entry = $this->get_entry($entry_id);

        if (false === $cached_entry) {
            exit(-1);
        }

        $entry = $cached_entry->get_entry();

        $shared_link = $this->get_shared_link($cached_entry, 'view');
        $embed_link = $this->get_embedded_link($cached_entry);

        return [
            'name' => $entry->get_name(),
            'extension' => $entry->get_extension(),
            'link' => $this->shorten_url($cached_entry, $shared_link),
            'embeddedlink' => $this->shorten_url($cached_entry, $embed_link),
            'size' => Helpers::bytes_to_size_1024($entry->get_size()),
            'error' => false,
        ];
    }

    public function create_shared_link($cached_entry, $type = 'view', $scope = false)
    {
        if (false === $scope) {
            $scope = ('personal' === $this->get_processor()->get_current_account()->get_type()) ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        $params = [
            'type' => $type,
            'scope' => $scope,
        ];

        try {
            $permission = $this->get_app()->get_drive()->items->createlink($cached_entry->get_id(), $params, ['driveId' => $cached_entry->get_drive_id()]);
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return esc_html__('The feature is disabled.', 'wpcloudplugins').' '.esc_html__('Please check the sharing permissions for the OneDrive / Sharepoint library to see if sharing is allowed.', 'wpcloudplugins');
        }

        $shared_link = $permission->getLink();
        $url = $shared_link->getWebUrl();

        // Get Expire date from url
        $url_array = parse_url($url);

        $expires = false;
        if (isset($url_array['query'])) {
            parse_str($url_array['query'], $url_attributes);
            $expires = isset($url_attributes['expiration']) ? $url_attributes['expiration'] : false;
        }

        $cached_entry->add_shared_link($url, $type, $scope, $expires);

        $this->get_cache()->set_updated();

        do_action('shareonedrive_log_event', 'shareonedrive_updated_metadata', $cached_entry, ['metadata_field' => 'Sharing Permissions']);

        return $url;
    }

    public function get_embedded_link($cached_entry, $scope = false)
    {
        if (false === $scope) {
            $scope = ('personal' === $this->get_processor()->get_current_account()->get_type()) ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if (false === $cached_entry->get_entry()->get_can_preview_by_cloud()) {
            return false;
        }

        /* For images, just return the actual file
         * BUG in API: embedded url of image files don't work
         */
        if (in_array($cached_entry->get_entry()->get_extension(), ['jpg', 'jpeg', 'gif', 'png'])) {
            return SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-embed-image&id='.$cached_entry->get_id();
        }

        /* Only OneDrive personal Accounts can create embedded links to documents
         * For the other accounts we need to add 'action=embedview' to the view url
         */

        if ('personal' === $this->get_processor()->get_current_account()->get_type()) {
            $embed_supported = ['csv', 'doc', 'docx', 'odp', 'ods', 'odt', 'pot', 'potm', 'potx', 'pps', 'ppsx', 'ppsxm', 'ppt', 'pptm', 'pptx', 'rtf', 'xlsx', 'pdf'];
            $embed = (in_array($cached_entry->get_entry()->get_extension(), $embed_supported));

            $embedded_link = $this->get_shared_link($cached_entry, 'embed', $scope);
            $embedded_link = str_replace('redir?', 'embed?', $embedded_link);

            if ($embed) {
                $embedded_link .= (false === strpos($embedded_link, '&em=2')) ? '&em=2' : ''; // Open embedded file directly
                $embedded_link .= '&wdHideHeaders=True';
                $embedded_link .= '&wdDownloadButton=False';
            }

            return $embedded_link;
        }

        // Business Accounts can use the Preview API to display PDF documents
        if ('pdf' === $cached_entry->get_entry()->get_extension()) {
            // Business accounts can use the Preview API */
            return SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-embed-redirect&id='.$cached_entry->get_id().'&account_id='.$cached_entry->get_account_id().'&drive_id='.$cached_entry->get_drive_id();
        }

        $embedded_link = $this->get_shared_link($cached_entry, 'view', $scope);
        $embedded_link .= (strpos($embedded_link, '?')) ? '&action=embedview' : '?action=embedview';

        return $embedded_link;
    }

    public function shorten_url(CacheNode $cached_entry, $url)
    {
        if (false !== strpos($url, 'localhost')) {
            // Most APIs don't support localhosts
            return $url;
        }

        try {
            switch ($this->get_processor()->get_setting('shortlinks')) {
                case 'Bit.ly':
                    $response = wp_remote_post('https://api-ssl.bitly.com/v4/shorten', [
                        'body' => json_encode(
                            [
                                'long_url' => $url,
                            ]
                        ),
                        'headers' => [
                            'Authorization' => 'Bearer '.$this->get_processor()->get_setting('bitly_apikey'),
                            'Content-Type' => 'application/json',
                        ],
                    ]);

                    $data = json_decode($response['body'], true);

                    return $data['link'];

                case 'Shorte.st':
                    $response = wp_remote_get('https://api.shorte'.'.st/s/'.$this->get_processor()->get_setting('shortest_apikey').'/'.$url);

                    $data = json_decode($response['body'], true);

                    return $data['shortenedUrl'];

                case 'Rebrandly':
                    $response = wp_remote_post('https://api.rebrandly.com/v1/links', [
                        'body' => json_encode(
                            [
                                'title' => $cached_entry->get_name(),
                                'destination' => $url,
                                'domain' => ['fullName' => $this->get_processor()->get_setting('rebrandly_domain')],
                            ]
                        ),
                        'headers' => [
                            'apikey' => $this->get_processor()->get_setting('rebrandly_apikey'),
                            'Content-Type' => 'application/json',
                            'workspace' => $this->get_processor()->get_setting('rebrandly_workspace'),
                        ],
                    ]);

                    $data = json_decode($response['body'], true);

                    return 'https://'.$data['shortUrl'];

                case 'None':
                default:
                    break;
            }
        } catch (\Exception $ex) {
            error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

            return $url;
        }

        return $url;
    }

    // Pull for changes

    public function get_changes($change_token = false, $drive_id = null)
    {
        if (empty($change_token)) {
            try {
                $result = $this->get_app()->get_drive()->changes->getlatest('root', ['driveId' => $drive_id]);
                preg_match("/delta.token=\\'?([a-zA-Z0-9;%\\-=_]+)\\'?/", $result['@odata.deltaLink'], $matches);
                $new_change_token = $result['@odata.deltaLink']; //$matches[1];
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return false;
            }
        }

        $changes = [];

        while (null != $change_token) {
            try {
                $expected_class = 'SODOneDrive_Service_Drive_Changes';

                $httpRequest = new \SODOneDrive_Http_Request($change_token, 'GET');
                $httpRequest = $this->get_app()->get_client()->getAuth()->sign($httpRequest);
                $httpRequest->setExpectedClass('SODOneDrive_Service_Drive_Changes');
                $result = $this->get_app()->get_client()->execute($httpRequest);

                if (isset($result['@odata.nextLink'])) {
                    $change_token = $result['@odata.nextLink'];
                } else {
                    $change_token = null;
                    $new_change_token = $result['@odata.deltaLink'];
                }

                $changes = array_merge($changes, $result->getValue());
            } catch (\Exception $ex) {
                error_log('[WP Cloud Plugin message]: '.sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));

                return false;
            }
        }

        $list_of_update_entries = [];
        foreach ($changes as $change) {
            if ('root' === $change->getName()) {
                continue;
            }

            // File is removed
            if (null !== $change->getDeleted()) {
                $list_of_update_entries[$change->getId()] = 'deleted';
            } else {
                // File is updated
                $list_of_update_entries[$change->getId()] = new Entry($change);
            }
        }

        return [$new_change_token, $list_of_update_entries];
    }

    /**
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor()
    {
        return $this->_processor;
    }

    /**
     * @return \TheLion\ShareoneDrive\Cache
     */
    public function get_cache()
    {
        return $this->get_processor()->get_cache();
    }

    /**
     * @return \TheLion\ShareoneDrive\App
     */
    public function get_app()
    {
        return $this->_app;
    }

    /**
     * @return \Graph_Client
     */
    public function get_library()
    {
        return $this->_app->get_client();
    }
}
