<?php

namespace TheLion\ShareoneDrive;

class Filebrowser
{
    /**
     * @var \TheLion\ShareoneDrive\Processor
     */
    private $_processor;
    private $_search = false;
    private $_parentfolders = [];

    public function __construct(Processor $_processor)
    {
        $this->_processor = $_processor;
    }

    /**
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor()
    {
        return $this->_processor;
    }

    public function getFilesList()
    {
        $this->_folder = $this->get_processor()->get_client()->get_folder();

        if ((false !== $this->_folder)) {
            $this->filesarray = $this->createFilesArray();
            $this->renderFilelist();
        } else {
            exit('Folder is not received');
        }
    }

    public function searchFiles()
    {
        $this->_search = true;
        $input = $_REQUEST['query'];
        $this->_folder = $this->get_processor()->get_client()->search_by_name($input);

        if ((false !== $this->_folder)) {
            $this->filesarray = $this->createFilesArray();
            $this->renderFilelist();
        }
    }

    public function setFolder($folder)
    {
        $this->_folder = $folder;
    }

    public function setParentFolder()
    {
        if (true === $this->_search) {
            return;
        }

        $currentfolder = $this->_folder['folder']->get_entry()->get_id();
        if ($currentfolder !== $this->get_processor()->get_root_folder()) {
            // Get parent folder from known folder path
            $cacheparentfolder = $this->get_processor()->get_client()->get_entry($this->get_processor()->get_root_folder());
            $folder_path = $this->get_processor()->get_folder_path();
            $parentid = end($folder_path);
            if (false !== $parentid) {
                $cacheparentfolder = $this->get_processor()->get_client()->get_entry($parentid);
            }

            /* Check if parent folder indeed is direct parent of entry
             * If not, return all known parents */
            $parentfolders = [];
            if (false !== $cacheparentfolder && $cacheparentfolder->has_children() && array_key_exists($currentfolder, $cacheparentfolder->get_children())) {
                $parentfolders[] = $cacheparentfolder->get_entry();
            } else {
                if ($this->_folder['folder']->has_parents()) {
                    foreach ($this->_folder['folder']->get_parents() as $parent) {
                        $parentfolders[] = $parent->get_entry();
                    }
                }
            }
            $this->_parentfolders = $parentfolders;
        }
    }

    public function renderFilelist()
    {
        // Create HTML Filelist
        $filelist_html = '';

        $breadcrumb_class = ('1' === $this->get_processor()->get_shortcode_option('show_breadcrumb')) ? 'has-breadcrumb' : 'no-breadcrumb';

        $filelist_html = "<div class='files {$breadcrumb_class}'>";
        $filelist_html .= "<div class='folders-container'>";

        if (count($this->filesarray) > 0) {
            $hasfilesorfolders = false;

            // Limit the number of files if needed
            if ('-1' !== $this->get_processor()->get_shortcode_option('max_files')) {
                $this->filesarray = array_slice($this->filesarray, 0, $this->get_processor()->get_shortcode_option('max_files'));
            }

            foreach ($this->filesarray as $item) {
                // Render folder div
                if ($item->is_dir()) {
                    $filelist_html .= $this->renderDir($item);

                    if (false === $item->is_parent_folder()) {
                        $hasfilesorfolders = true;
                    }
                }
            }
        }

        if (false === $this->_search && false === $this->_folder['folder']->is_virtual_folder()) {
            $filelist_html .= $this->renderNewFolder();
        }

        $filelist_html .= "</div><div class='files-container'>";

        if (count($this->filesarray) > 0) {
            foreach ($this->filesarray as $item) {
                // Render files div
                if ($item->is_file()) {
                    $filelist_html .= $this->renderFile($item);
                    $hasfilesorfolders = true;
                }
            }

            if (false === $hasfilesorfolders) {
                if ('1' === $this->get_processor()->get_shortcode_option('show_files')) {
                    $filelist_html .= $this->renderNoResults();
                }
            }
        } else {
            if ('1' === $this->get_processor()->get_shortcode_option('show_files') || true === $this->_search) {
                $filelist_html .= $this->renderNoResults();
            }
        }

        $filelist_html .= '</div></div>';

        // Create HTML Filelist title
        $file_path = '<ol class="wpcp-breadcrumb">';
        $userfolder = $this->get_processor()->get_user_folders()->get_auto_linked_folder_for_user();
        $folder_path = $this->get_processor()->get_folder_path();
        $root_folder_id = $this->get_processor()->get_root_folder();
        $current_id = $this->_folder['folder']->get_entry()->get_id();
        $drive_id = $this->_folder['folder']->get_drive_id();

        if ($root_folder_id === $current_id) {
            $file_path .= "<li class='first-breadcrumb'><a href='#{$current_id}' class='folder current_folder' data-id='".$current_id."' data-drive-id='{$drive_id}'>".$this->get_processor()->get_shortcode_option('root_text').'</a></li>';
        } elseif (false === $this->_search || 'parent' === $this->get_processor()->get_shortcode_option('searchfrom')) {
            foreach ($folder_path as $parent_id) {
                if ($parent_id === $root_folder_id) {
                    $file_path .= "<li class='first-breadcrumb'><a href='#{$parent_id}' class='folder' data-id='".$parent_id."' data-drive-id=''>".$this->get_processor()->get_shortcode_option('root_text').'</a></li>';
                } else {
                    $parent_folder = $this->get_processor()->get_client()->get_folder($parent_id);
                    $file_path .= "<li><a href='#{$parent_id}' class='folder' data-id='".$parent_id."' data-drive-id='".$parent_folder['folder']->get_drive_id()."'>".$parent_folder['folder']->get_name().'</a></li>';
                }
            }
            $file_path .= "<li><a href='#{$current_id}' class='folder current_folder' data-id='".$current_id."' data-drive-id='".$this->_folder['folder']->get_drive_id()."'>".$this->_folder['folder']->get_entry()->get_name().'</a></li>';
        }

        if (true === $this->_search) {
            $file_path .= "<li><a href='javascript:void(0)' class='folder'>".sprintf(esc_html__('Results for %s', 'wpcloudplugins'), "'".$_REQUEST['query']."'").'</a></li>';
        }

        $file_path .= '</ol>';

        $raw_path = '';
        if ((true !== $this->_search) && (current_user_can('edit_posts') || current_user_can('edit_pages')) && ('true' == get_user_option('rich_editing'))) {
            $raw_path = $this->_folder['folder']->get_entry()->get_name();
        }

        // lastFolder contains current folder path of the user
        if (true !== $this->_search && (end($folder_path) !== $this->_folder['folder']->get_entry()->get_id())) {
            $folder_path[] = $this->_folder['folder']->get_entry()->get_id();
        }

        if (true === $this->_search) {
            $lastFolder = $this->get_processor()->get_last_folder();
        } else {
            $lastFolder = $this->_folder['folder']->get_entry()->get_id();
        }

        $response = json_encode([
            'rawpath' => $raw_path,
            'folderPath' => base64_encode(json_encode($folder_path)),
            'driveId' => $this->_folder['folder']->get_drive_id(),
            'accountId' => $this->_folder['folder']->get_account_id(),
            'virtual' => false === $this->_search && in_array($this->_folder['folder']->get_virtual_folder(), ['drives', 'sites']),
            'lastFolder' => $lastFolder,
            'breadcrumb' => $file_path,
            'html' => $filelist_html,
            'hasChanges' => defined('HAS_CHANGES'),
        ]);

        if (false === defined('HAS_CHANGES')) {
            $cached_request = new CacheRequest($this->get_processor());
            $cached_request->add_cached_response($response);
        }

        echo $response;

        exit();
    }

    public function renderNoResults()
    {
        $icon_set = $this->get_processor()->get_setting('loaders');

        $html = "<div class='entry file no-entries'>\n";
        $html .= "<div class='entry_block'>\n";
        $html .= "<div class='entry_thumbnail'><div class='entry_thumbnail-view-bottom'><div class='entry_thumbnail-view-center'>\n";
        $html .= "<a class='entry_link'><img class='preloading' src='".SHAREONEDRIVE_ROOTPATH."/css/images/transparant.png' data-src='".$icon_set['no_results']."' data-src-retina='".$icon_set['no_results']."'/></a>";
        $html .= "</div></div></div>\n";

        $html .= "<div class='entry-info'>";
        $html .= "<div class='entry-info-name'>";
        $html .= "<a class='entry_link' title='".esc_html__('This folder is empty', 'wpcloudplugins')."'><div class='entry-name-view'>";
        $html .= '<span>'.esc_html__('This folder is empty', 'wpcloudplugins').'</span>';
        $html .= '</div></a>';
        $html .= "</div>\n";

        $html .= "</div>\n";
        $html .= "</div>\n";
        $html .= "</div>\n";

        return $html;
    }

    public function renderDir(Entry $item)
    {
        $return = '';

        $classmoveable = ($this->get_processor()->get_user()->can_move_folders() || $this->get_processor()->get_user()->can_move_folders()) ? 'moveable' : '';
        $isparent = (isset($this->_folder['folder'])) ? $this->_folder['folder']->is_in_folder($item->get_id()) : false;

        $return .= "<div class='entry {$classmoveable} folder ".($isparent ? 'pf' : '')."' data-id='".$item->get_id()."' data-drive-id='".$item->get_drive_id()."' data-name='".htmlspecialchars($item->get_basename(), ENT_QUOTES | ENT_HTML401, 'UTF-8')."'>\n";
        if (!$isparent) {
            if ('linkto' === $this->get_processor()->get_shortcode_option('mcepopup') || 'linktobackendglobal' === $this->get_processor()->get_shortcode_option('mcepopup')) {
                $return .= "<div class='entry_linkto'>\n";
                $return .= '<span>'."<input class='button-secondary' type='submit' title='".esc_html__('Select folder', 'wpcloudplugins')."' value='".esc_html__('Select folder', 'wpcloudplugins')."'>".'</span>';
                $return .= '</div>';
            }
        }

        $return .= "<div class='entry_block'>\n";
        $return .= "<div class='entry-info'>";

        $thumburl = $isparent ? SHAREONEDRIVE_ICON_SET.'256x256/prev.png' : $item->get_icon_large();
        $return .= "<div class='entry-info-icon'><div class='preloading'></div><img class='preloading' src='".SHAREONEDRIVE_ROOTPATH."/css/images/transparant.png' data-src='{$thumburl}' data-src-retina='{$thumburl}'/></div>";

        $return .= "<div class='entry-info-name'>";
        $return .= "<a class='entry_link' title='{$item->get_basename()}'>";
        $return .= '<span>';
        $return .= (($isparent) ? '<strong>'.esc_html__('Previous folder', 'wpcloudplugins').'</strong>' : $item->get_name()).' </span>';
        $return .= '</span>';
        $return .= '</a></div>';

        if (!$isparent) {
            $return .= $this->renderDescription($item);
            $return .= $this->renderActionMenu($item);
            $return .= $this->renderCheckBox($item);
        }

        $return .= "</div>\n";

        $return .= "</div>\n";
        $return .= "</div>\n";

        return $return;
    }

    public function renderFile(Entry $item)
    {
        $link = $this->renderFileNameLink($item);
        $title = $link['filename'].((('1' === $this->get_processor()->get_shortcode_option('show_filesize')) && ($item->get_size() > 0)) ? ' ('.Helpers::bytes_to_size_1024($item->get_size()).')' : '&nbsp;');

        $crop = 'none'; //($this->get_processor()->get_current_account()->get_type() === 'personal') ? 'none' : 'center'; Is now working for Bussines Accounts as well?
        $thumbnail_medium = $item->get_thumbnail_with_size(500, 500, $crop);

        $classmoveable = ($this->get_processor()->get_user()->can_move_files()) ? 'moveable' : '';
        $has_tooltip = ($item->has_own_thumbnail() && !empty($thumbnail_medium) && ('shortcode' !== $this->get_processor()->get_shortcode_option('mcepopup')) && ('1' === $this->get_processor()->get_shortcode_option('hover_thumbs'))) ? "data-tooltip=''" : '';

        $return = '';
        $return .= "<div class='entry file {$classmoveable}' data-id='".$item->get_id()."' data-name='".htmlspecialchars($item->get_basename(), ENT_QUOTES | ENT_HTML401, 'UTF-8')."' {$has_tooltip}>\n";
        $return .= "<div class='entry_block'>\n";

        $return .= "<div class='entry_thumbnail'><div class='entry_thumbnail-view-bottom'><div class='entry_thumbnail-view-center'>\n";

        $return .= "<div class='preloading'></div>";
        $return .= "<img referrerPolicy='no-referrer' class='preloading' src='".SHAREONEDRIVE_ROOTPATH."/css/images/transparant.png' data-src='".$thumbnail_medium."' data-src-retina='".$thumbnail_medium."' data-src-backup='".$item->get_icon_large()."'/>";
        $return .= "</div></div></div>\n";

        if ($duration = $item->get_media('duration')) {
            $return .= "<div class='entry-duration'><i class='fas fa-play fa-xs'></i> ".Helpers::convert_ms_to_time($duration).'</div>';
        }

        $return .= "<div class='entry-info'>";

        $return .= "<div class='entry-info-icon'><img src='".$item->get_icon()."'/></div>";
        $return .= "<div class='entry-info-name'>";
        $return .= '<a '.$link['url'].' '.$link['target']." class='entry_link ".$link['class']."' ".$link['onclick']." title='".$title."' ".$link['lightbox']." data-filename='".$link['filename']."' data-entry-id='{$item->get_id()}' >";
        $return .= '<span>'.$link['filename'].'</span>';
        $return .= '</a>';

        if (('shortcode' === $this->get_processor()->get_shortcode_option('mcepopup')) && (in_array($item->get_extension(), ['mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'mp3', 'm4a', 'oga', 'wav', 'webm']))) {
            $return .= "&nbsp;<a class='entry_media_shortcode'><i class='fas fa-code'></i></a>";
        }

        $return .= '</div>';

        $return .= $this->renderModifiedDate($item);
        $return .= $this->renderSize($item);
        $return .= $this->renderDescription($item);
        $return .= $this->renderActionMenu($item);
        $return .= $this->renderCheckBox($item);
        $return .= "</div>\n";

        $return .= $link['lightbox_inline'];

        $return .= "</div>\n";
        $return .= "</div>\n";

        return $return;
    }

    public function renderSize(EntryAbstract $item)
    {
        if ('1' === $this->get_processor()->get_shortcode_option('show_filesize')) {
            $size = ($item->get_size() > 0) ? Helpers::bytes_to_size_1024($item->get_size()) : '&nbsp;';

            return "<div class='entry-info-size entry-info-metadata'>".$size.'</div>';
        }
    }

    public function renderModifiedDate(EntryAbstract $item)
    {
        if ('1' === $this->get_processor()->get_shortcode_option('show_filedate')) {
            return "<div class='entry-info-modified-date entry-info-metadata'>".$item->get_last_edited_str().'</div>';
        }
    }

    public function renderCheckBox(EntryAbstract $item)
    {
        $checkbox = '';

        if ($item->is_dir()) {
            if ($this->get_processor()->get_user()->can_download_zip() || $this->get_processor()->get_user()->can_delete_folders() || $this->get_processor()->get_user()->can_move_folders()) {
                $checkbox .= "<div class='entry-info-button entry_checkbox'><input type='checkbox' name='selected-files[]' class='selected-files' value='".$item->get_id()."' id='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'/><label for='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'></label></div>";
            }

            if ((in_array($this->get_processor()->get_shortcode_option('mcepopup'), ['links', 'embedded']))) {
                $checkbox .= "<div class='entry-info-button entry_checkbox'><input type='checkbox' name='selected-files[]' class='selected-files' value='".$item->get_id()."' id='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'/><label for='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'></label></div>";
            }
        } else {
            if ($this->get_processor()->get_user()->can_download_zip() || $this->get_processor()->get_user()->can_delete_files() || $this->get_processor()->get_user()->can_move_files()) {
                $checkbox .= "<div class='entry-info-button entry_checkbox'><input type='checkbox' name='selected-files[]' class='selected-files' value='".$item->get_id()."' id='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'/><label for='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'></label></div>";
            }

            if ((in_array($this->get_processor()->get_shortcode_option('mcepopup'), ['links', 'embedded']))) {
                $checkbox .= "<div class='entry-info-button entry_checkbox'><input type='checkbox' name='selected-files[]' class='selected-files' value='".$item->get_id()."' id='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'/><label for='checkbox-{$this->get_processor()->get_listtoken()}-{$item->get_id()}'></label></div>";
            }
        }

        return $checkbox;
    }

    public function renderFileNameLink(Entry $item)
    {
        $class = '';
        $url = '';
        $target = '';
        $onclick = '';
        $lightbox = '';
        $lightbox_inline = '';
        $datatype = 'iframe';
        $filename = ('1' === $this->get_processor()->get_shortcode_option('show_ext')) ? $item->get_name() : $item->get_basename();

        // Check if user is allowed to preview the file
        $usercanpreview = $this->get_processor()->get_user()->can_preview() && '1' === $this->get_processor()->get_shortcode_option('allow_preview') && 'preview' === $this->get_processor()->get_shortcode_option('onclick');
        if (
                $item->is_dir()
                || false === $item->get_can_preview_by_cloud()
                || 'zip' === $item->get_extension()
                || false === $this->get_processor()->get_user()->can_view()
        ) {
            $usercanpreview = false;
        }

        if ($usercanpreview && ('0' === $this->get_processor()->get_shortcode_option('mcepopup'))) {
            $url = SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-preview&id='.($item->get_id()).'&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$this->_folder['folder']->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken();

            // Display Direct links for image and media files
            if (in_array($item->get_extension(), ['jpg', 'jpeg', 'gif', 'png'])) {
                $datatype = 'image';
                if ($this->get_processor()->get_client()->has_temporarily_link($item)) {
                    $url = $this->get_processor()->get_client()->get_temporarily_link($item);
                    //} elseif ($this->get_processor()->get_client()->has_shared_link($item)) {
                    //    $url = $this->get_processor()->get_client()->get_shared_link($item) . '?raw=1';
                }
            } elseif (in_array($item->get_extension(), ['mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'mp3', 'm4a', 'ogg', 'oga'])) {
                $datatype = 'inline';
                if ($this->get_processor()->get_client()->has_temporarily_link($item)) {
                    $url = $this->get_processor()->get_client()->get_temporarily_link($item);
                }
            }

            // Check if we need to preview inline
            if ('1' === $this->get_processor()->get_shortcode_option('previewinline')) {
                $class = 'entry_link ilightbox-group';
                $onclick = "sendAnalyticsSOD('Preview', '{$item->get_name()}');";

                // Lightbox Settings
                $lightbox = "rel='ilightbox[".$this->get_processor()->get_listtoken()."]' ";
                $lightbox .= 'data-type="'.$datatype.'"';

                switch ($datatype) {
                    case 'image':
                        $lightbox .= ' data-options="thumbnail: \''.$item->get_thumbnail_icon().'\'"';

                        break;

                    case 'inline':
                        $id = 'ilightbox_'.$this->get_processor()->get_listtoken().'_'.md5($item->get_id());
                        $html5_element = (false === strpos($item->get_mimetype(), 'video')) ? 'audio' : 'video';
                        $icon = str_replace('32x32', '128x128', $item->get_thumbnail_icon());
                        $thumbnail = $item->get_thumbnail_large();

                         $lightbox_size = (false !== strpos($item->get_mimetype(), 'audio')) ? 'width: \'85%\',' : 'width: \'85%\', height: \'85%\',';
                        $lightbox .= ' data-options="mousewheel: false, swipe:false, '.$lightbox_size.' thumbnail: \''.$thumbnail.'\'"';

                        $download = 'controlsList="nodownload"';
                        $lightbox_inline = '<div id="'.$id.'" class="html5_player" style="display:none;"><'.$html5_element.' controls '.$download.' preload="metadata"  poster="'.$item->get_thumbnail_large().'"> <source data-src="'.$url.'" type="'.$item->get_mimetype().'">'.esc_html__('Your browser does not support HTML5. You can only download this file', 'wpcloudplugins').'</'.$html5_element.'></div>';
                        $url = '#'.$id;

                        break;

                    case 'iframe':
                        $lightbox .= ' data-options="mousewheel: false, width: \'85%\', height: \'80%\', thumbnail: \''.str_replace('32x32', '128x128', $item->get_thumbnail_icon()).'\'"';
                        // no break
                    default:
                        break;
                }
            } else {
                $url .= '&inline=0';
                $class = 'entry_action_external_view';
                $target = '_blank';
                $onclick = "sendAnalyticsSOD('Preview  (new window)', '{$item->get_name()}');";
            }
        } elseif (('0' === $this->get_processor()->get_shortcode_option('mcepopup')) && $this->get_processor()->get_user()->can_download()) {
            // Check if user is allowed to download file
            $url = SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-download&id='.($item->get_id()).'&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$this->_folder['folder']->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken();
            $class = 'entry_action_download';

            $target = ('url' === $item->get_extension()) ? '"_blank"' : $target;

            if ('redirect' === $this->get_processor()->get_shortcode_option('onclick')) {
                $url .= '&redirect=1';
                $target = '_blank';
                $class = 'entry_action_external_view';
            }
        }
        // No Url

        if ('woocommerce' === $this->get_processor()->get_shortcode_option('mcepopup')) {
            $class = 'entry_woocommerce_link';
        }

        if ('shortcode' === $this->get_processor()->get_shortcode_option('mcepopup')) {
            $url = '';
        }

        if (!empty($url)) {
            $url = "href='".$url."'";
        }
        if (!empty($target)) {
            $target = "target='".$target."'";
        }
        if (!empty($onclick)) {
            $onclick = 'onclick="'.$onclick.'"';
        }

        return ['filename' => htmlspecialchars($filename, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES, 'UTF-8'), 'class' => $class, 'url' => $url, 'lightbox' => $lightbox, 'lightbox_inline' => $lightbox_inline, 'target' => $target, 'onclick' => $onclick];
    }

    public function renderDescription(Entry $item)
    {
        $html = '';

        if ($item->is_virtual_folder()) {
            return $html;
        }

        $has_description = (false === empty($item->description));

        $metadata = [
            'modified' => "<i class='fas fa-history'></i> ".$item->get_last_edited_str(),
            'size' => ($item->get_size() > 0) ? Helpers::bytes_to_size_1024($item->get_size()) : '',
        ];

        $html .= "<div class='entry-info-button entry-description-button ".(($has_description) ? '-visible' : '')."' tabindex='0'><i class='fas fa-info-circle'></i>\n";
        $html .= "<div class='tippy-content-holder'>";
        $html .= "<div class='description-textbox'>";
        $html .= ($has_description) ? "<div class='description-text'>".nl2br($item->get_description()).'</div>' : '';
        $html .= "<div class='description-file-info'>".implode(' &bull; ', array_filter($metadata)).'</div>';

        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    public function renderActionMenu(Entry $item)
    {
        $html = '';

        if ($item->is_virtual_folder()) {
            return $html;
        }

        $usercanpreview = $this->get_processor()->get_user()->can_preview() && '1' === $this->get_processor()->get_shortcode_option('allow_preview');
        if (
                $item->is_dir()
                || false === $item->get_can_preview_by_cloud()
                || 'zip' === $item->get_extension()
                || false === $this->get_processor()->get_user()->can_view()
        ) {
            $usercanpreview = false;
        }

        $usercanshare = $this->get_processor()->get_user()->can_share();
        $usercanread = $this->get_processor()->get_user()->can_download();
        $usercanedit = $this->get_processor()->get_user()->can_edit();
        $usercaneditdescription = $this->get_processor()->get_user()->can_edit_description();

        $usercandeeplink = $this->get_processor()->get_user()->can_deeplink();
        $usercanrename = ($item->is_dir()) ? $this->get_processor()->get_user()->can_rename_folders() : $this->get_processor()->get_user()->can_rename_files();
        $usercanmove = ($item->is_dir()) ? $this->get_processor()->get_user()->can_move_folders() : $this->get_processor()->get_user()->can_move_files();
        $usercancopy = (($item->is_dir()) ? $this->get_processor()->get_user()->can_copy_folders() : $this->get_processor()->get_user()->can_copy_files());
        $usercandelete = ($item->is_dir()) ? $this->get_processor()->get_user()->can_delete_folders() : $this->get_processor()->get_user()->can_delete_files();

        $filename = $item->get_basename();
        $filename .= (('1' === $this->get_processor()->get_shortcode_option('show_ext') && !empty($item->extension)) ? '.'.$item->get_extension() : '');

        // View
        if ($usercanpreview) {
            if (('1' === $this->get_processor()->get_shortcode_option('previewinline') && 'preview' === $this->get_processor()->get_shortcode_option('onclick'))) {
                $html .= "<li><a class='entry_action_view' title='".esc_html__('Preview', 'wpcloudplugins')."'><i class='fas fa-eye'></i>&nbsp;".esc_html__('Preview', 'wpcloudplugins').'</a></li>';
            }
            $url = SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-preview&inline=0&id='.urlencode($item->get_id()).'&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$this->_folder['folder']->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken();
            $onclick = "sendAnalyticsSOD('Preview (new window)', '".$item->get_basename().((!empty($item->extension)) ? '.'.$item->get_extension() : '')."');";

            if ($usercanread) {
                $html .= "<li><a href='{$url}' target='_blank' class='entry_action_external_view' onclick=\"{$onclick}\" title='".esc_html__('Preview in new window', 'wpcloudplugins')."'><i class='fas fa-desktop'></i>&nbsp;".esc_html__('Preview in new window', 'wpcloudplugins').'</a></li>';
            }
        }

        // Deeplink
        if ($usercandeeplink) {
            $html .= "<li><a class='entry_action_deeplink' title='".esc_html__('Direct link', 'wpcloudplugins')."'><i class='fas fa-link'></i>&nbsp;".esc_html__('Direct link', 'wpcloudplugins').'</a></li>';
        }

        // Shortlink
        if ($usercanshare) {
            $html .= "<li><a class='entry_action_shortlink' title='".esc_html__('Share', 'wpcloudplugins')."'><i class='fas fa-share-alt'></i>&nbsp;".esc_html__('Share', 'wpcloudplugins').'</a></li>';
        }

        // Download
        if (($usercanread) && ($item->is_file())) {
            $target = ('url' === $item->get_extension()) ? '"_blank"' : '';
            $html .= "<li><a href='".SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-download&id='.$item->get_id().'&dl=1&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$this->_folder['folder']->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken()."' class='entry_action_download' download='".$filename."' {$target} data-filename='".$filename."' title='".esc_html__('Download', 'wpcloudplugins')."'><i class='fas fa-arrow-down'></i>&nbsp;".esc_html__('Download', 'wpcloudplugins').'</a></li>';
        }

        if ($usercanread && $item->is_dir() && '1' === $this->get_processor()->get_shortcode_option('can_download_zip')) {
            $html .= "<li><a class='entry_action_download' data-filename='".$filename."' title='".esc_html__('Download', 'wpcloudplugins')."'><i class='fas fa-arrow-down'></i>&nbsp;".esc_html__('Download', 'wpcloudplugins').'</a></li>';
        }

        // Exportformats
        if (($usercanread) && ($item->is_file()) && (count($item->get_save_as()) > 0)) {
            foreach ($item->get_save_as() as $name => $exportlinks) {
                $html .= "<li><a href='".SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-download&id='.$item->get_id().'&dl=1&extension='.$exportlinks['extension'].'&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$this->_folder['folder']->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken()."' target='_blank' class='entry_action_export' data-filename='".$filename."'><i class='fa ".$exportlinks['icon']."'></i>&nbsp;".esc_html__('Download as', 'wpcloudplugins').' '.$name.'</a>';
            }
        }

        // Edit
        if (($usercanedit) && ($item->is_file()) && $item->get_can_edit_by_cloud()) {
            $html .= "<li><a href='".SHAREONEDRIVE_ADMIN_URL.'?action=shareonedrive-edit&id='.$item->get_id().'&account_id='.$this->_folder['folder']->get_account_id().'&drive_id='.$this->_folder['folder']->get_drive_id().'&listtoken='.$this->get_processor()->get_listtoken()."' target='_blank' class='entry_action_edit' data-filename='".$filename."' title='".esc_html__('Edit (new window)', 'wpcloudplugins')."'><i class='fas fa-pen-square'></i>&nbsp;".esc_html__('Edit (new window)', 'wpcloudplugins').'</a></li>';
        }

        // Descriptions
        if ($usercaneditdescription) {
            if (empty($item->description)) {
                $html .= "<li><a class='entry_action_description' title='".esc_html__('Add description', 'wpcloudplugins')."'><i class='fas fa-comment-alt'></i>&nbsp;".esc_html__('Add description', 'wpcloudplugins').'</a></li>';
            } else {
                $html .= "<li><a class='entry_action_description' title='".esc_html__('Edit description', 'wpcloudplugins')."'><i class='fas fa-comment-alt'></i>&nbsp;".esc_html__('Edit description', 'wpcloudplugins').'</a></li>';
            }
        }

        // Rename
        if ($usercanrename) {
            $html .= "<li><a class='entry_action_rename' title='".esc_html__('Rename', 'wpcloudplugins')."'><i class='fas fa-tag'></i>&nbsp;".esc_html__('Rename', 'wpcloudplugins').'</a></li>';
        }
        // Move
        if ($usercanmove) {
            $html .= "<li><a class='entry_action_move' title='".esc_html__('Move to', 'wpcloudplugins')."'><i class='fas fa-folder-open'></i>&nbsp;".esc_html__('Move to', 'wpcloudplugins').'</a></li>';
        }

        // Copy
        if ($usercancopy) {
            $html .= "<li><a class='entry_action_copy' title='".esc_html__('Make a copy', 'wpcloudplugins')."'><i class='fas fa-clone'></i>&nbsp;".esc_html__('Make a copy', 'wpcloudplugins').'</a></li>';
        }

        // Delete
        if ($usercandelete) {
            $html .= "<li><a class='entry_action_delete' title='".esc_html__('Delete', 'wpcloudplugins')."'><i class='fas fa-trash'></i>&nbsp;".esc_html__('Delete', 'wpcloudplugins').'</a></li>';
        }

        if ('' !== $html) {
            return "<div class='entry-info-button entry-action-menu-button' title='".esc_html__('More actions', 'wpcloudplugins')."' tabindex='0'><i class='fas fa-ellipsis-v'></i><div id='menu-".$item->get_id()."' class='entry-action-menu-button-content tippy-content-holder'><ul data-id='".$item->get_id()."' data-name='".$item->get_basename()."'>".$html."</ul></div></div>\n";
        }

        return $html;
    }

    public function renderNewFolder()
    {
        $return = '';

        if (
            false === $this->get_processor()->get_user()->can_add_folders()
            || true === $this->_search
            || '1' === $this->get_processor()->get_shortcode_option('show_breadcrumb')
            ) {
            return $return;
        }

        $icon_set = $this->get_processor()->get_setting('icon_set');

        $return .= "<div class='entry folder newfolder'>\n";
        $return .= "<div class='entry_block'>\n";
        $return .= "<div class='entry_thumbnail'><div class='entry_thumbnail-view-bottom'><div class='entry_thumbnail-view-center'>\n";
        $return .= "<a class='entry_link'><img class='preloading' src='".SHAREONEDRIVE_ROOTPATH."/css/images/transparant.png'  data-src='".$icon_set.'128x128/folder-new.png'."' data-src-retina='".$icon_set.'256x256/folder-new.png'."'/></a>";
        $return .= "</div></div></div>\n";

        $return .= "<div class='entry-info'>";
        $return .= "<div class='entry-info-name'>";
        $return .= "<a class='entry_link' title='".esc_html__('Add folder', 'wpcloudplugins')."'><div class='entry-name-view'>";
        $return .= '<span>'.esc_html__('Add folder', 'wpcloudplugins').'</span>';
        $return .= '</div></a>';
        $return .= "</div>\n";

        $return .= "</div>\n";
        $return .= "</div>\n";
        $return .= "</div>\n";

        return $return;
    }

    public function createFilesArray()
    {
        $filesarray = [];

        $this->setParentFolder();

        //Add folders and files to filelist
        if (isset($this->_folder['contents']) && count($this->_folder['contents']) > 0) {
            foreach ($this->_folder['contents'] as $node) {
                // Check if entry is allowed
                if (!$this->get_processor()->_is_entry_authorized($node)) {
                    continue;
                }
                $filesarray[] = $node->get_entry();
            }

            $filesarray = $this->get_processor()->sort_filelist($filesarray);
        }

        // Add 'back to Previous folder' if needed
        if (isset($this->_folder['folder'])) {
            $folder = $this->_folder['folder']->get_entry();
            $add_parent_folder_item = true;

            if ($this->_search || $folder->get_id() === $this->get_processor()->get_root_folder()) {
                $add_parent_folder_item = false;
            }

            if ($add_parent_folder_item) {
                foreach ($this->_parentfolders as $parentfolder) {
                    array_unshift($filesarray, $parentfolder);
                }
            }
        }

        return $filesarray;
    }
}
