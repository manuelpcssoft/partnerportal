<?php
$network_wide_authorization = $this->get_processor()->is_network_authorized();
?>
<div class="shareonedrive admin-settings">
  <form id="shareonedrive-options" method="post" action="<?php echo network_admin_url('edit.php?action='.$this->plugin_network_options_key); ?>">
    <?php settings_fields('share_one_drive_settings'); ?>
    <input type="hidden" name="share_one_drive_settings[onedrive_account_type]" id="onedrive_account_type" value="<?php echo @esc_attr($this->settings['onedrive_account_type']); ?>" >

    <div class="wrap">
      <div class="shareonedrive-header">
        <div class="shareonedrive-logo"><a href="https://www.wpcloudplugins.com" target="_blank"><img src="<?php echo SHAREONEDRIVE_ROOTPATH; ?>/css/images/wpcp-logo-dark.svg" height="64" width="64"/></a></div>
        <div class="shareonedrive-form-buttons" style="<?php echo (false === is_plugin_active_for_network(SHAREONEDRIVE_SLUG)) ? 'display:none;' : ''; ?>"> <div id="wpcp-save-settings-button" class="simple-button default"><?php esc_html_e('Save Settings', 'wpcloudplugins'); ?>&nbsp;<div class='wpcp-spinner'></div></div></div>
        <div class="shareonedrive-title"><?php esc_html_e('Settings', 'wpcloudplugins'); ?></div>
      </div>


      <div id="" class="shareonedrive-panel shareonedrive-panel-left">      
                <div class="shareonedrive-nav-header"><?php esc_html_e('Settings', 'wpcloudplugins'); ?> <a href="<?php echo admin_url('update-core.php'); ?>">(Ver: <?php echo SHAREONEDRIVE_VERSION; ?>)</a></div>

        <ul class="shareonedrive-nav-tabs">
          <li id="settings_general_tab" data-tab="settings_general" class="current"><a ><?php esc_html_e('General', 'wpcloudplugins'); ?></a></li>
          <?php if ($network_wide_authorization) { ?>
              <li id="settings_advanced_tab" data-tab="settings_advanced" ><a ><?php esc_html_e('Advanced', 'wpcloudplugins'); ?></a></li>
          <?php } ?>
          <li id="settings_system_tab" data-tab="settings_system" ><a><?php esc_html_e('System information', 'wpcloudplugins'); ?></a></li>
          <li id="settings_help_tab" data-tab="settings_help" ><a><?php esc_html_e('Support', 'wpcloudplugins'); ?></a></li>
        </ul>

        <div class="shareonedrive-nav-header" style="margin-top: 50px;"><?php esc_html_e('Other Cloud Plugins', 'wpcloudplugins'); ?></div>
        <ul class="shareonedrive-nav-tabs">
          <li id="settings_help_tab" data-tab="settings_help"><a href="https://1.envato.market/L6yXj" target="_blank" style="color:#522058;">Google Drive <i class="fas fa-external-link-square-alt" aria-hidden="true"></i></a></li>
          <li id="settings_help_tab" data-tab="settings_help"><a href="https://1.envato.market/vLjyO" target="_blank" style="color:#522058;">Dropbox <i class="fas fa-external-link-square-alt" aria-hidden="true"></i></a></li>
          <li id="settings_help_tab" data-tab="settings_help"><a href="https://1.envato.market/M4B53" target="_blank" style="color:#522058;">Box <i class="fas fa-external-link-square-alt" aria-hidden="true"></i></a></li>
        </ul> 


                    <div class="shareonedrive-nav-footer">
          <a href="https://www.wpcloudplugins.com/" target="_blank">
            <img alt="" height="auto" src="<?php echo SHAREONEDRIVE_ROOTPATH; ?>/css/images/wpcloudplugins-logo-dark.png">
          </a>
        </div>
      </div>

      <div class="shareonedrive-panel shareonedrive-panel-right">
        <!-- General Tab -->
        <div id="settings_general" class="shareonedrive-tab-panel current">

          <div class="shareonedrive-tab-panel-header"><?php esc_html_e('General', 'wpcloudplugins'); ?></div>

          <div class="shareonedrive-option-title"><?php esc_html_e('Plugin License', 'wpcloudplugins'); ?></div>
          <?php
          echo $this->get_plugin_activated_box();
          ?>

          <?php if (is_plugin_active_for_network(SHAREONEDRIVE_SLUG)) { ?>
              <div class="shareonedrive-option-title"><?php esc_html_e('Network Wide Authorization', 'wpcloudplugins'); ?>
                <div class="shareonedrive-onoffswitch">
                  <input type='hidden' value='No' name='share_one_drive_settings[network_wide]'/>
                  <input type="checkbox" name="share_one_drive_settings[network_wide]" id="wpcp-network_wide-button" class="shareonedrive-onoffswitch-checkbox" <?php echo (empty($network_wide_authorization)) ? '' : 'checked="checked"'; ?> data-div-toggle="network_wide"/>
                  <label class="shareonedrive-onoffswitch-label" for="wpcp-network_wide-button"></label>
                </div>
              </div>


              <?php
              if ($network_wide_authorization) {
                  ?>
                  <div class="shareonedrive-option-title"><?php esc_html_e('Accounts', 'wpcloudplugins'); ?></div>
                  <div class="shareonedrive-accounts-list">
                    <?php
                    $app = $this->get_app();
                  $app->get_client()->setPrompt('select_account');
                  $app->get_client()->setAccessType('offline');
                  $app->get_client()->setApprovalPrompt('login');

                  $authurl = $app->get_auth_url();
                  $personal_url = str_replace('common', 'consumers', $authurl);
                  $business_url = str_replace('common', 'organizations', $authurl); ?>
                    <div class='account account-new'>
                      <img class='account-image' src='<?php echo SHAREONEDRIVE_ROOTPATH; ?>/css/images/onedrive_logo.png'/>
                      <div class='account-info-container'>
                        <div class='account-info'>
                          <div class="account-info-name">
                            <?php esc_html_e('Link a new account to the plugin', 'wpcloudplugins'); ?>
                          </div>
                          <span class="account-info-space"><a href="#" id="wpcp-read-privacy-policy"><i class="fas fa-shield-alt"></i> <?php esc_html_e('What happens with my data when I authorize the plugin?', 'wpcloudplugins'); ?></a></span>   
                          <div class='account-actions' style="float:none; margin-top:10px;">
                            <div type='button' class='wpcp-add-account-button simple-button blue' data-url="<?php echo $personal_url; ?>" title="<?php esc_html_e('Add OneDrive', 'wpcloudplugins'); ?>"><i class='fas fa-plus-circle' aria-hidden='true'></i>&nbsp;OneDrive</div>
                            <div type='button' class='wpcp-add-account-button simple-button blue' data-url="<?php echo $business_url; ?>" title="<?php esc_html_e('Add OneDrive Business', 'wpcloudplugins'); ?>"><i class='fas fa-plus-circle' aria-hidden='true'></i>&nbsp;OneDrive Business</div>
                          </div>                          
                        </div>
                      </div>
                    </div>
                    <?php
                    foreach ($this->get_main()->get_accounts()->list_accounts() as $account_id => $account) {
                        echo $this->get_plugin_authorization_box($account);
                    } ?>
                  </div>
                  <?php
              }
              ?>

              <?php
          }
          ?>

        </div>
        <!-- End General Tab -->


        <!--  Advanced Tab -->
        <?php if ($network_wide_authorization) { ?>
            <div id="settings_advanced"  class="shareonedrive-tab-panel">
              <div class="shareonedrive-tab-panel-header"><?php esc_html_e('Advanced', 'wpcloudplugins'); ?></div>

              <div class="shareonedrive-option-title"><?php esc_html_e('"Lost Authorization" notification', 'wpcloudplugins'); ?></div>
              <div class="shareonedrive-option-description"><?php esc_html_e('If the plugin somehow loses its authorization, a notification email will be send to the following email address', 'wpcloudplugins'); ?>:</div>
              <input class="shareonedrive-option-input-large" type="text" name="share_one_drive_settings[lostauthorization_notification]" id="lostauthorization_notification" value="<?php echo esc_attr($this->settings['lostauthorization_notification']); ?>">  


              <div class="shareonedrive-option-title"><?php esc_html_e('Own App', 'wpcloudplugins'); ?>
                <div class="shareonedrive-onoffswitch">
                  <input type='hidden' value='No' name='share_one_drive_settings[onedrive_app_own]'/>
                  <input type="checkbox" name="share_one_drive_settings[onedrive_app_own]" id="onedrive_app_own" class="shareonedrive-onoffswitch-checkbox" <?php echo (empty($this->settings['onedrive_app_client_id']) || empty($this->settings['onedrive_app_client_secret'])) ? '' : 'checked="checked"'; ?> data-div-toggle="own-app"/>
                  <label class="shareonedrive-onoffswitch-label" for="onedrive_app_own"></label>
                </div>
              </div>

              <div class="shareonedrive-suboptions own-app <?php echo (empty($this->settings['onedrive_app_client_id']) || empty($this->settings['onedrive_app_client_secret'])) ? 'hidden' : ''; ?> ">
                <div class="shareonedrive-option-description">
                  <strong>Using your own OneDrive App is <u>optional</u></strong>. For an easy setup you can just use the default App of the plugin itself by leaving the ID and Secret empty. The advantage of using your own app is limited. If you decided to create your own OneDrive App anyway, please enter your settings. In the <a href="https://florisdeleeuwnl.zendesk.com/hc/en-us/articles/205059105-How-do-I-create-my-own-OneDrive-App-" target="_blank">documentation</a> you can find how you can create a OneDrive App.
                  <br/><br/>
                  <div class="sod-warning">
                    <i><strong>NOTICE</strong>: If you encounter any issues when trying to use your own App, please fall back on the default App by disabling this setting.</i>
                  </div>
                </div>

                <div class="shareonedrive-option-title">OneDrive Client ID</div>
                <div class="shareonedrive-option-description"><?php echo esc_html__('Only if you want to use your own App, insert your Client ID here', 'wpcloudplugins'); ?>.</div>
                <input class="shareonedrive-option-input-large" type="text" name="share_one_drive_settings[onedrive_app_client_id]" id="onedrive_app_client_id" value="<?php echo esc_attr($this->settings['onedrive_app_client_id']); ?>" placeholder="<--- <?php echo esc_html__('Leave empty for easy setup', 'wpcloudplugins'); ?> --->" >

                <div class="shareonedrive-option-title">OneDrive Client secret</div>
                <div class="shareonedrive-option-description"><?php echo esc_html__('If you want to use your own App, insert your Client Secret here', 'wpcloudplugins'); ?>.</div>
                <input class="shareonedrive-option-input-large" type="text" name="share_one_drive_settings[onedrive_app_client_secret]" id="onedrive_app_client_secret" value="<?php echo esc_attr($this->settings['onedrive_app_client_secret']); ?>" placeholder="<--- <?php echo esc_html__('Leave empty for easy setup', 'wpcloudplugins'); ?> --->" >   
              </div>


              <div class="shareonedrive-option-title"><?php esc_html_e('Business Accounts', 'wpcloudplugins'); ?> | <?php esc_html_e('Scope shared-links', 'wpcloudplugins'); ?></div>
              <div class="shareonedrive-option-description"><?php echo wp_kses(__('Who should be able to access the links that are created by the plugin? If set to <strong>Public</strong> the links will be accessible by anyone. <strong>Within Organization</strong> will make links accessible within your organization only. Anonymous links may be disabled by the tenant administrator', 'wpcloudplugins'), ['strong' => []]); ?>.</div>
              <select type="text" name="share_one_drive_settings[link_scope]" id="link_scope">
                <option value="anonymous" <?php echo 'anonymous' === $this->settings['link_scope'] ? "selected='selected'" : ''; ?>>Public</option>
                <option value="organization" <?php echo 'organization' === $this->settings['link_scope'] ? "selected='selected'" : ''; ?>>Within Organization</option>
              </select>

            </div>
        <?php } ?>
        <!-- End Advanced Tab -->

        <!-- System info Tab -->
        <div id="settings_system"  class="shareonedrive-tab-panel">
          <div class="shareonedrive-tab-panel-header"><?php esc_html_e('System information', 'wpcloudplugins'); ?></div>
          <?php echo $this->get_system_information(); ?>
        </div>
        <!-- End System info -->

        <!-- Help Tab -->
        <div id="settings_help"  class="shareonedrive-tab-panel">

          <div class="shareonedrive-tab-panel-header"><?php esc_html_e('Support', 'wpcloudplugins'); ?></div>
          <div class="shareonedrive-option-title"><?php esc_html_e('Support & Documentation', 'wpcloudplugins'); ?></div>
          <div id="message">
            <p><?php esc_html_e('Check the documentation of the plugin in case you encounter any problems or are looking for support.', 'wpcloudplugins'); ?></p>
            <div id='wpcp-open-docs-button' type='button' class='simple-button blue'><?php esc_html_e('Open Documentation', 'wpcloudplugins'); ?></div>
          </div>
          <br/>
          <div class="shareonedrive-option-title"><?php esc_html_e('Cache', 'wpcloudplugins'); ?></div>
          <?php echo $this->get_plugin_reset_cache_box(); ?>

        </div>  
      </div>
      <!-- End Help info -->
    </div>
  </form>

  <!-- End Privacy Policy -->
  <div id="wpcp-privacy-policy" style='clear:both;display:none'>  
    <div class="shareonedrive shareonedrive-tb-content">
      <div class="shareonedrive-option-title"><?php esc_html_e('Requested scopes and justification', 'wpcloudplugins'); ?></div>
      <div class="shareonedrive-option-description"> <?php echo sprintf(esc_html__('In order to display your content stored on %s, you have to authorize it with your %s account.', 'wpcloudplugins'), 'OneDrive & SharePoint', 'Microsoft'); ?> <?php _e('The authorization will ask you to grant the application the following scopes:', 'wpcloudplugins'); ?>

      <br/><br/>
      <table class="widefat">
        <thead>
          <tr>
            <th>Scope</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>        
          <tr>
            <td><code>files.readwrite.all</code></td>
            <td><?php echo sprintf(esc_html__('Allow the plugin to see, edit, create, and delete all of your %s files', 'wpcloudplugins'), 'OneDrive'); ?>.</td>
          </tr>
          <tr>
            <td><code>sites.readwrite.all</code></td>
            <td><?php echo sprintf(esc_html__('Allow the plugin to see, edit, create, and delete all of your %s files', 'wpcloudplugins'), 'SharePoint'); ?></td>
          </tr>
          <tr>
            <td><code>offline_access</code></td>
            <td><?php (esc_html_e('Allow the plugin to maintain access to the content on behalf of the user.', 'wpcloudplugins')); ?></td>
          </tr>
          <tr>
            <td><code>user.read</code></td>
            <td><?php (esc_html_e('Allow the plugin to see your publicly available personal info, like email, name and profile picture. This information will only be displayed on this page for easy account identification.', 'wpcloudplugins')); ?></td>
          </tr>
        </tbody>
      </table>

      <br/>
      <div class="shareonedrive-option-title"><?php esc_html_e('Information about the data', 'wpcloudplugins'); ?></div>
      The authorization tokens will be stored, encrypted, on this server and is not accessible by the developer or any third party. When you use the Application, all communications are strictly between your server and the cloud storage service servers. The communication is encrypted and the communication will not go through WP Cloud Plugins servers. We do not collect and do not have access to your personal data.
      
      <br/><br/>
      <i class="fas fa-shield-alt"></i> <?php echo sprintf(esc_html__('Read the full %sPrivacy Policy%s if you have any further privacy concerns.', 'wpcloudplugins'), '<a href="https://www.wpcloudplugins.com/privacy-policy/privacy-policy-share-one-drive/">', '</a>'); ?></div>
    </div>
  </div>
  <!-- End Short Privacy Policy -->

</div>