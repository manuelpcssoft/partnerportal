=== Share-One-Drive ===
Requires at least: 5.0
Tested up to: 5.7.2
Requires PHP: 7.0

Share-one-Drive is the #1 Ultimate OneDrive plugin for WordPress plugin on the market and part of a series of Cloud Plugins already powering 5.000+ company websites improving their workflow. Join now and start using your OneDrive even more efficiently by integrating it on your website!

== Description ==

This plugin will help you to easily integrate OneDrive Personal, OneDrive Business and Sharepoint Libraries into your WordPress website or blog. Share-one-Drive allows you to view, download, delete, rename files & folders directly from a WordPress page. You can use Share-one-Drive as a File browser, Gallery, Audio- or Video-Player!

== Changelog ==
You can find the Release Notes in the [Documentation](https://www.wpcloudplugins.com/wp-content/plugins/share-one-drive/_documentation/index.html#releasenotes).

== Upgrade Notice ==

In case you have multiple WP Cloud Plugins active on your site, please make sure they are all up to date to prevent compatibility issues between the plugins.

= 1.14 =
SHAREPOINT SUPPORT: This version introduced support for Sharepoint Libraries. In order to use this, you will have to re-authorize the plugin again with your OneDrive Business account if it was linked before version 1.14

= 1.12.15 =
IMPORTANT: This version implements token encryption to patch a vulnerability on NGINX servers. A re-authorization might be needed in some cases
