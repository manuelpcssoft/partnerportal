<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * WooFunnels integration class.
 *
 * @since 3.37.14
 */
class WPF_WooFunnels extends WPF_Integrations_Base {

	/**
	 * The slug name for WP Fusion's module tracking.
	 *
	 * @since 3.37.14
	 * @var  slug
	 */

	public $slug = 'woofunnels';

	/**
	 * The integration name.
	 *
	 * @since 3.37.14
	 * @var  name
	 */

	public $name = 'WooFunnels';

	/**
	 * Get things started.
	 *
	 * @since 3.37.14
	 */
	public function init() {

		/**
		 * Settings related hooks
		 */

		add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );

		add_action( 'wffn_optin_action_tabs', array( $this, 'optin_tab' ) );
		add_action( 'wffn_optin_action_tabs_content', array( $this, 'optin_tab_content' ) );
		add_action( 'wfopp_default_actions_settings', array( $this, 'optin_default' ) );
		add_action( 'wfopp_localized_data', array( $this, 'optin_localize' ) );
		add_action( 'admin_footer', array( $this, 'optin_settings_js' ), 9999 );

		add_action( 'woocommerce_order_status_wfocu-pri-order', array( wp_fusion()->integrations->woocommerce, 'woocommerce_apply_tags_checkout' ) );

		/**
		 * Process optin hook
		 */
		add_action( 'wffn_optin_form_submit', array( $this, 'handle_optin_submission' ), 10, 2 );

		/**
		 * Settings related hooks
		 */
		add_action( 'admin_enqueue_scripts', array( $this, 'upsell_localize' ), 100 );
		add_filter( 'wfocu_offer_settings_default', array( $this, 'upsell_defaults' ) );
		add_action( 'admin_footer', array( $this, 'upsells_settings_js' ) );

		/**
		 * Process Upsell Hook
		 */
		add_action( 'wfocu_offer_accepted_and_processed', array( $this, 'handle_upsell_accept' ), 10, 1 );
		add_action( 'wfocu_offer_rejected_event', array( $this, 'handle_upsell_reject' ), 10, 1 );

	}

	/**
	 * Enqueue multiselect scripts.
	 *
	 * @since 3.37.14
	 */
	public function scripts() {

		if ( WFOCU_Core()->admin->is_upstroke_page( 'offers' ) ) {

			wp_enqueue_style( 'wffn-vue-multiselect', WFFN_Core()->get_plugin_url() . '/admin/assets/vuejs/vue-multiselect.min.css', array(), WFFN_VERSION_DEV );

			wp_enqueue_script( 'wffn-vuejs', WFFN_Core()->get_plugin_url() . '/admin/assets/vuejs/vue.min.js', array(), '2.6.10' );
			wp_enqueue_script( 'wffn-vue-vfg', WFFN_Core()->get_plugin_url() . '/admin/assets/vuejs/vfg.min.js', array(), '2.3.4' );
			wp_enqueue_script( 'wffn-vue-multiselect', WFFN_Core()->get_plugin_url() . '/admin/assets/vuejs/vue-multiselect.min.js', array(), WFFN_VERSION_DEV );

		}

	}

	/**
	 * Render optin settings tab.
	 *
	 * @since 3.37.14
	 */
	public function optin_tab() {
		?>
		<div class="wffn-tab-title wffn-tab-desktop-title" data-tab="4" role="tab"><?php esc_html_e( 'WP Fusion', 'wp-fusion' ); ?></div>
		<?php
	}

	/**
	 * Render optin tab content.
	 *
	 * @since 3.37.14
	 */
	public function optin_tab_content() {
		?>
		<vue-form-generator ref="learndash_ref" :schema="schemaFusion" :model="modelFusion" :options="formOptions"></vue-form-generator>
		<?php
	}

	/**
	 * Register optin defaults.
	 *
	 * @since  3.37.14
	 *
	 * @param  array $actions_defaults The actions defaults.
	 * @return array The actions defaults.
	 */
	public function optin_default( $actions_defaults ) {
		$actions_defaults['op_wpfusion_optin_tags'] = [];
		$actions_defaults['op_wpfusion_enable']     = 'false';

		return $actions_defaults;
	}


	/**
	 * Prepare the available tags (optins).
	 *
	 * @since  3.37.14
	 *
	 * @param  array $data   The localize data.
	 * @return array The localize data.
	 */
	public function optin_localize( $data ) {
		$all_available_tags = wp_fusion()->settings->get_available_tags_flat();

		foreach ( $all_available_tags as $id => $label ) {

			$options[] = array(
				'id'   => $id,
				'name' => $label,
			);

		}
		$data['op_wpfusion_optin_tags_vals'] = $options;

		$data['op_wpfusion_optin_radio_vals'] = array(
			array(
				'value' => 'true',
				'name'  => __( 'Yes', 'wp-fusion' ),
			),
			array(
				'value' => 'false',
				'name'  => __( 'No', 'wp-fusion' ),
			),
		);

		return $data;
	}

	/**
	 * Prepare the available tags (upsell).
	 *
	 * @since 3.37.14
	 */
	public function upsell_localize() {
		if ( WFOCU_Core()->admin->is_upstroke_page( 'offers' ) ) {

			$data               = [];
			$all_available_tags = wp_fusion()->settings->get_available_tags_flat();

			foreach ( $all_available_tags as $id => $label ) {

				$options[] = array(
					'id'   => $id,
					'name' => $label,
				);

			}
			$data['wpfusion_tags'] = $options;

			wp_localize_script( 'wfocu-admin', 'wfocuWPF', $data );
		}

	}

	/**
	 * JS for the optin settings.
	 *
	 * @since 3.37.14
	 */
	public function optin_settings_js() {

		?>
		<script>
			(function ($) {
				$(document).ready(function () {
					if (typeof window.wffnBuilderCommons !== "undefined") {

						window.wffnBuilderCommons.addFilter('wffn_js_optin_vue_data', function (e) {
							let custom_settings_valid_fields = [
								{
									type: "radios",
									label: "<?php _e( 'Enable Integration', 'wp-fusion' ); ?>",
									model: "op_wpfusion_enable",
									values: () => {
										return wfop_action.op_wpfusion_optin_radio_vals
									},
									hint: "<?php printf( __( 'Select Yes to sync optins with %s.', 'wp-fusion' ), wp_fusion()->crm->name ); ?>",
								},
								{
									type: "vueMultiSelect",
									label: "<?php _e( 'Apply Tags - Optin Submitted', 'wp-fusion' ); ?>",
									placeholder: "<?php _e( 'Select tags', 'wp-fusion' ); ?>",
									model: "op_wpfusion_optin_tags",
									selectOptions: {hideNoneSelectedText: true},
									hint: "<?php printf( __( 'Select tags to be applied in %s when this form is submitted.', 'wp-fusion' ), wp_fusion()->crm->name ); ?>",
									values: () => {
										return wfop_action.op_wpfusion_optin_tags_vals
									},
									selectOptions: {
										multiple: true,
										key: "id",
										label: "name",
									},
									visible: function (model) {
										return (model.op_wpfusion_enable === 'true');
									},
								},


							];

							e.schemaFusion = {
								groups: [{
									legend: '<?php _e( 'WP Fusion', 'wp-fusion' ); ?>',
									fields: custom_settings_valid_fields
								}]
							};
							e.modelFusion = wfop_action.action_options;
							return e;
						});
					}
				});


			})(jQuery);

		</script>
		<?php
	}

	/**
	 * Upsell defaults.
	 *
	 * @since  3.37.14
	 *
	 * @param  object $object The object.
	 * @return object The object.
	 */
	public function upsell_defaults( $object ) {
		$object->wfocu_wpfusion_offer_accept_tags = [];
		$object->wfocu_wpfusion_offer_reject_tags = [];

		return $object;
	}

	/**
	 * JS for the upsell settings.
	 *
	 * @since 3.37.14
	 */
	public function upsells_settings_js() {
		?>
		<script>

			(function ($, doc, win) {
				'use strict';

				if (typeof window.wfocuBuilderCommons !== "undefined") {
					Vue.component('multiselect', window.VueMultiselect.default);
					window.wfocuBuilderCommons.addFilter('wfocu_offer_settings', function (e) {
						e.unshift(
							{
								type: "label",
								label: "<?php _e( 'Apply Tags - Offer Accepted', 'wp-fusion' ); ?>",
								model: "wfocu_wp_fusion_label_1",

							},
							{
								type: "vueMultiSelect",
								label: "",
								model: "wfocu_wpfusion_offer_accept_tags",
								placeholder: "<?php _e( 'Select tags', 'wp-fusion' ); ?>",
								selectOptions: {hideNoneSelectedText: true},
								values: () => {
									return wfocuWPF.wpfusion_tags
								},
								selectOptions: {
									multiple: true,
									key: "id",
									label: "name",
								}
							},

							{
								type: "label",
								label: "<?php _e( 'Apply Tags - Offer Rejected', 'wp-fusion' ); ?>",
								model: "wfocu_wp_fusion_label_2",

							},
							{
								type: "vueMultiSelect",
								label: "",
								model: "wfocu_wpfusion_offer_reject_tags",
								placeholder: "<?php _e( 'Select tags', 'wp-fusion' ); ?>",
								selectOptions: {hideNoneSelectedText: true},
								values: () => {
									return wfocuWPF.wpfusion_tags
								},
								selectOptions: {
									multiple: true,
									key: "id",
									label: "name",
								}
							},
						);


						return e;
					});
				}

			})(jQuery, document, window);
		</script>
		<?php
	}

	/**
	 * Handles an optin submission.
	 *
	 * @since 3.37.14
	 *
	 * @param int   $optin_id    The optin ID.
	 * @param array $posted_data The posted data.
	 */
	public function handle_optin_submission( $optin_id, $posted_data ) {

		$settings = WFOPP_Core()->optin_actions->get_optin_action_settings( $optin_id );

		if ( 'true' == $settings['op_wpfusion_enable'] ) {

			// Map data

			$field_map = array(
				'optin_first_name' => 'first_name',
				'optin_last_name'  => 'last_name',
				'optin_email'      => 'user_email',
			);

			$update_data = $this->map_meta_fields( $posted_data, $field_map );
			$update_data = wp_fusion()->crm_base->map_meta_fields( $update_data );

			// Prep tags

			$apply_tags = array();

			if ( ! empty( $settings['op_wpfusion_optin_tags'] ) ) {

				foreach ( $settings['op_wpfusion_optin_tags'] as $tag ) {
					$apply_tags[] = $tag['id'];
				}
			}

			$args = array(
				'email_address'    => $posted_data['optin_email'],
				'update_data'      => $update_data,
				'apply_tags'       => $apply_tags,
				'integration_slug' => 'woofunnels_optin',
				'integration_name' => 'WooFunnels Optin',
				'form_id'          => $optin_id,
				'form_title'       => get_the_title( $optin_id ),
				'form_edit_link'   => admin_url( 'admin.php?page=wf-op&section=action&edit=' . $optin_id ),
			);

			$contact_id = WPF_Forms_Helper::process_form_data( $args );

		}

	}

	/**
	 * Handle an upsell accept.
	 *
	 * @since 3.37.14
	 *
	 * @param int $offer_id The offer ID.
	 */
	public function handle_upsell_accept( $offer_id ) {

		$get_offer_data = WFOCU_Core()->data->get( '_current_offer' );

		if ( empty( $get_offer_data->settings->wfocu_wpfusion_offer_accept_tags ) ) {
			return;
		}

		$offer_tags = array();

		foreach ( $get_offer_data->settings->wfocu_wpfusion_offer_accept_tags as $tag ) {
			$offer_tags[] = $tag['id'];
		}

		$order = WFOCU_Core()->data->get( 'porder', false, '_orders' );

		$user_id = $order->get_user_id();

		if ( ! empty( $user_id ) ) {

			wp_fusion()->user->apply_tags( $offer_tags, $user_id );

		} else {

			$contact_id = $order->get_meta( wp_fusion()->crm->slug . '_contact_id' );

			if ( ! empty( $contact_id ) ) {
				wpf_log( 'info', 0, 'Applying Offer Accepted tags to guest contact ID ' . $contact_id . ': ', array( 'tag_array' => $offer_tags ) );
				wp_fusion()->crm->apply_tags( $offer_tags, $contact_id );
			}
		}

	}

	/**
	 * Handle an upsell reject.
	 *
	 * @since 3.37.14
	 *
	 * @param array $args   The arguments.
	 */
	public function handle_upsell_reject( $args ) {

		$get_offer_data = WFOCU_Core()->data->get( '_current_offer' );

		if ( empty( $get_offer_data->settings->wfocu_wpfusion_offer_reject_tags ) ) {
			return;
		}

		$offer_tags = array();

		foreach ( $get_offer_data->settings->wfocu_wpfusion_offer_reject_tags as $tag ) {
			$offer_tags[] = $tag['id'];
		}

		$order = WFOCU_Core()->data->get( 'porder', false, '_orders' );

		$user_id = $order->get_user_id();

		if ( ! empty( $user_id ) ) {

			wp_fusion()->user->apply_tags( $offer_tags, $user_id );

		} else {

			$contact_id = $order->get_meta( wp_fusion()->crm->slug . '_contact_id' );

			if ( ! empty( $contact_id ) ) {

				wpf_log( 'info', 0, 'Applying Offer Rejected tags to guest contact ID ' . $contact_id . ': ', array( 'tag_array' => $offer_tags ) );
				wp_fusion()->crm->apply_tags( $offer_tags, $contact_id );
			}
		}
	}

}

new WPF_WooFunnels();
