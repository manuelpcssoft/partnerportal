<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class WPF_Tribe_Tickets extends WPF_Integrations_Base {

	/**
	 * Gets things started
	 *
	 * @access  public
	 * @return  void
	 */

	public function init() {

		$this->slug = 'tribe-tickets';

		$this->name = 'Tribe Tickets';

		// Making Custom contact fields for WPF settings
		add_filter( 'wpf_meta_field_groups', array( $this, 'add_meta_field_group' ), 10 );
		add_filter( 'wpf_meta_fields', array( $this, 'prepare_meta_fields' ), 20 );

		add_filter( 'wpf_compatibility_notices', array( $this, 'compatibility_notices' ) );

		// Query filtering
		add_filter( 'tribe_query_can_inject_date_field', array( $this, 'can_inject_date_field' ), 10, 2 );
		add_filter( 'wpf_should_filter_query', array( $this, 'should_filter_query' ), 10, 2 );
		add_action( 'tribe_repository_events_pre_get_posts', array( $this, 'filter_event_queries' ) );

		// Moving one attendee to another event
		add_action( 'tribe_tickets_ticket_moved', array( $this, 'tickets_ticket_moved' ), 10, 6 );

		// Saving in post_meta
		add_action( 'event_tickets_after_save_ticket', array( $this, 'tickets_after_save_ticket' ), 10, 4 );
		add_action( 'wp_ajax_wpf_tribe_tickets_save', array( $this, 'ajax_save_ticket' ) );

		// Metabox
		add_action( 'tribe_events_tickets_metabox_advanced', array( $this, 'tickets_metabox' ), 10, 2 );
		add_action( 'tribe_events_tickets_metabox_edit_main', array( $this, 'tickets_metabox_new' ), 10, 2 );

		// Transfering and preparing ticket/rsvp/edd info to be able to get picked up by CRM
		add_action( 'event_tickets_rsvp_ticket_created', array( $this, 'rsvp_ticket_created' ), 20, 4 );

		// Push ticket meta for EDD tickets after purchase
		add_action( 'event_tickets_edd_ticket_created', array( $this, 'edd_ticket_created' ), 20, 4 ); // 20 so the ticket meta is saved

		// Push event date for WooCommere tickets after purchase
		add_action( 'event_tickets_woocommerce_ticket_created', array( $this, 'woocommerce_ticket_created' ), 20, 4 ); // 20 so the ticket meta is saved

		// Sync check-ins
		add_action( 'rsvp_checkin', array( $this, 'checkin' ), 10, 2 );
		add_action( 'eddtickets_checkin', array( $this, 'checkin' ), 10, 2 );
		add_action( 'wootickets_checkin', array( $this, 'checkin' ), 10, 2 );

	}

	/**
	 * Adds field group for Tribe Tickets to contact fields list
	 *
	 * @access  public
	 * @return  array Meta fields
	 */

	public function add_meta_field_group( $field_groups ) {

		$field_groups['tribe_events_event'] = array(
			'title'  => 'The Events Calendar - Event',
			'fields' => array(),
		);

		$field_groups['tribe_events_attendee'] = array(
			'title'  => 'The Events Calendar - Attendee',
			'fields' => array(),
		);

		return $field_groups;

	}

	/**
	 * Sets field labels and types for event fields
	 *
	 * @access  public
	 * @return  array Meta fields
	 */

	public function prepare_meta_fields( $meta_fields ) {

		$meta_fields['ticket_name'] = array(
			'label' => 'Ticket Name',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_name'] = array(
			'label' => 'Event Name',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_date'] = array(
			'label' => 'Event Date',
			'type'  => 'date',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_time'] = array(
			'label' => 'Event Time',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['venue_name'] = array(
			'label' => 'Venue Name',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_address'] = array(
			'label' => 'Event Address',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_city'] = array(
			'label' => 'Event City',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_state'] = array(
			'label' => 'Event State',
			'type'  => 'state',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_province'] = array(
			'label' => 'Event Province',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_country'] = array(
			'label' => 'Event Country',
			'type'  => 'country',
			'group' => 'tribe_events_event',
		);

		$meta_fields['event_zip'] = array(
			'label' => 'Event Zip',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['organizer_name'] = array(
			'label' => 'Organizer Name',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['organizer_phone'] = array(
			'label' => 'Organizer Phone',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['organizer_website'] = array(
			'label' => 'Organizer Website',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		$meta_fields['organizer_email'] = array(
			'label' => 'Organizer Email',
			'type'  => 'text',
			'group' => 'tribe_events_event',
		);

		// Custom event fields

		$custom_fields = tribe_get_option( 'custom-fields' );

		if ( ! empty( $custom_fields ) ) {

			foreach ( $custom_fields as $field ) {

				$meta_fields[ $field['name'] ] = array(
					'label' => $field['label'],
					'type'  => $field['type'],
					'group' => 'tribe_events_event',
				);

			}
		}

		$meta_fields['event_checkin'] = array(
			'label' => 'Event Checkin',
			'type'  => 'checkbox',
			'group' => 'tribe_events_attendee',
		);

		$args = array(
			'post_type'    => array( 'download', 'tribe_rsvp_tickets', 'product' ),
			'nopaging'     => true,
			'fields'       => 'ids',
			'meta_key'     => '_tribe_tickets_meta',
			'meta_compare' => 'EXISTS',
		);

		$tickets = get_posts( $args );

		if ( empty( $tickets ) ) {
			return $meta_fields;
		}

		foreach ( $tickets as $post_id ) {

			$event_fields = get_post_meta( $post_id, '_tribe_tickets_meta', true );

			if ( empty( $event_fields ) ) {
				continue;
			}

			foreach ( $event_fields as $field ) {

				$meta_fields[ $field['slug'] ] = array(
					'label' => $field['label'],
					'type'  => $field['type'],
					'group' => 'tribe_events_attendee',
				);

			}
		}

		return $meta_fields;

	}


	/**
	 * Show warning if Query Filtering is being used with cached calendar view.
	 *
	 * @since  3.37.0
	 *
	 * @param  array $notices The notices.
	 * @return array The notices.
	 */
	public function compatibility_notices( $notices ) {

		if ( wp_fusion()->settings->get( 'hide_archives' ) ) {

			$types = wp_fusion()->settings->get( 'query_filter_post_types', array() );

			if ( empty( $types ) || in_array( 'tribe_events', $types ) ) {

				if ( tribe_get_option( 'enable_month_view_cache', false ) ) {

					$notices['tribe-events-cache'] = sprintf(
						__( '<strong>Note:</strong> You have Filter Queries enabled on the <code>tribe_events</code> post type, but the <strong>Month View Cache</strong> is enabled in your <a href="%s">Event Display Settings</a>. To use Query Filtering with events in month view, you must turn off the month view cache.', 'wp-fusion' ),
						admin_url( 'edit.php?post_type=tribe_events&page=tribe-common&tab=display' )
					);

				}
			}
		}

		return $notices;

	}


	/**
	 * Disable date field injection on filtered queries.
	 *
	 * Filter Queries - Advanced creates a database error when searching for
	 * events and Tribe is injecting date meta parameters into the query. To get
	 * around that we'll disable date injection if the query is being filtered.
	 *
	 * The error is:
	 *
	 * WordPress database error Unknown column 'wp_postmeta.meta_value' in
	 * 'field list' for query SELECT SQL_CALC_FOUND_ROWS DISTINCT wp_posts.*,
	 * MIN(wp_postmeta.meta_value) as EventStartDate,
	 * MIN(tribe_event_end_date.meta_value) as EventEndDate FROM wp_posts  LEFT
	 * JOIN wp_postmeta as tribe_event_end_date ON ( wp_posts.ID =
	 * tribe_event_end_date.post_id AND tribe_event_end_date.meta_key =
	 * '_EventEndDate' )  WHERE 1=1  AND wp_posts.ID NOT IN (8197) AND
	 * wp_posts.post_type = 'tribe_events' AND ((wp_posts.post_status =
	 * 'publish'))  ORDER BY wp_posts.post_date DESC LIMIT 0
	 *
	 * @since  3.37.3
	 *
	 * @param  boolean  $can_inject Whether the date field can be injected.
	 * @param  WP_Query $query      Query object.
	 * @return bool     Whether the date field can be injected.
	 */
	public function can_inject_date_field( $can_inject, $query ) {

		if ( $query->get( 'wpf_filtering_query' ) ) {
			$can_inject = false;
		}

		return $can_inject;
	}

	/**
	 * Bypass Filter Queries on Events in calendar view (handled by the next function).
	 *
	 * @since  3.37.6
	 *
	 * @param  bool     $filter Whether or not to filter the query.
	 * @param  WP_Query $query  The query.
	 * @return bool     Whether or not to filter the query.
	 */
	public function should_filter_query( $filter, $query ) {

		if ( did_action( 'tribe_repository_events_pre_get_posts' ) && 'tribe_events' == $query->get( 'post_type' ) ) {
			return false;
		}

		return $filter;

	}

	/**
	 * Makes Filter Queries work with events.
	 *
	 * @since 3.37.6
	 *
	 * @param WP_Query $query  The query.
	 */
	public function filter_event_queries( &$query ) {

		if ( wp_fusion()->settings->get( 'hide_archives' ) && wp_fusion()->access->is_post_type_eligible_for_query_filtering( 'tribe_events' ) ) {

			$not_in = wp_fusion()->access->get_restricted_posts( 'tribe_events' );

			if ( ! empty( $not_in ) ) {

				// Maybe merge existing

				if ( ! empty( $query->get( 'post__not_in' ) ) ) {
					$not_in = array_merge( $query->get( 'post__not_in' ), $not_in );
				}

				$query->set( 'post__not_in', $not_in );

				$query->set( 'wpf_filtering_query', true );

				// If the query has a post__in, that will take priority, so we'll adjust for that here

				if ( ! empty( $query->get( 'post__in' ) ) ) {
					$in = array_diff( $query->get( 'post__in' ), $not_in );
					$query->set( 'post__in', $in );
				}
			}
		}

	}

	/**
	 * Gets all the attendee and event meta from an attendee ID
	 *
	 * @access  public
	 * @return  array Update data
	 */

	public function get_attendee_meta( $attendee_id ) {

		// Get the event ID (Tribe annoyingly stores these all in different keys)

		$event_id  = get_post_meta( $attendee_id, '_tribe_wooticket_event', true );
		$ticket_id = get_post_meta( $attendee_id, '_tribe_wooticket_product', true );

		if ( empty( $event_id ) ) {
			$event_id  = get_post_meta( $attendee_id, '_tribe_eddticket_event', true );
			$ticket_id = get_post_meta( $attendee_id, '_tribe_eddticket_product', true );
		}

		if ( empty( $event_id ) ) {
			$event_id  = get_post_meta( $attendee_id, '_tribe_rsvp_event', true );
			$ticket_id = get_post_meta( $attendee_id, '_tribe_rsvp_product', true );
		}

		$attendee_email = get_post_meta( $attendee_id, '_tribe_tickets_email', true );

		if ( empty( $attendee_email ) ) {
			$attendee_email = get_post_meta( $attendee_id, '_tribe_rsvp_email', true );
		}

		$venue_id       = get_post_meta( $event_id, '_EventVenueID', true );
		$event_date     = get_post_meta( $event_id, '_EventStartDate', true );
		$event_address  = get_post_meta( $venue_id, '_VenueAddress', true );
		$event_city     = get_post_meta( $venue_id, '_VenueCity', true );
		$event_country  = get_post_meta( $venue_id, '_VenueCountry', true );
		$event_state    = get_post_meta( $venue_id, '_VenueState', true );
		$event_province = get_post_meta( $venue_id, '_VenueProvince', true );
		$event_zip      = get_post_meta( $venue_id, '_VenueZip', true );

		$event_time = date( 'g:ia', strtotime( $event_date ) );

		$update_data = array(
			'user_email'     => $attendee_email,
			'ticket_name'    => get_the_title( $ticket_id ),
			'event_name'     => get_the_title( $event_id ),
			'event_date'     => $event_date,
			'event_time'     => $event_time,
			'venue_name'     => get_the_title( $venue_id ),
			'event_address'  => $event_address,
			'event_city'     => $event_city,
			'event_state'    => $event_state,
			'event_province' => $event_province,
			'event_country'  => $event_country,
			'event_zip'      => $event_zip,
		);

		// Name

		$full_name = get_post_meta( $attendee_id, '_tribe_tickets_full_name', true );

		if ( empty( $full_name ) ) {
			$full_name = get_post_meta( $attendee_id, '_tribe_rsvp_full_name', true );
		}

		if ( ! empty( $full_name ) ) {

			$parts                     = explode( ' ', $full_name, 2 );
			$update_data['first_name'] = $parts[0];

			if ( isset( $parts[1] ) ) {
				$update_data['last_name'] = $parts[1];
			}
		}

		// Organizer

		$organizer_id = get_post_meta( $event_id, '_EventOrganizerID', true );

		if ( ! empty( $organizer_id ) ) {

			$organizer_data = array(
				'organizer_name'    => get_the_title( $organizer_id ),
				'organizer_phone'   => get_post_meta( $organizer_id, '_OrganizerPhone', true ),
				'organizer_website' => get_post_meta( $organizer_id, '_OrganizerWebsite', true ),
				'organizer_email'   => get_post_meta( $organizer_id, '_OrganizerEmail', true ),
			);

			$update_data = array_merge( $update_data, $organizer_data );

		}

		$ticket_meta = get_post_meta( $attendee_id, '_tribe_tickets_meta', true );

		if ( ! empty( $ticket_meta ) ) {
			$update_data = array_merge( $update_data, $ticket_meta );
		}

		// Possible additional event meta

		$event_meta = get_post_meta( $event_id );

		foreach ( $event_meta as $key => $value ) {

			if ( 0 === strpos( $key, '_ecp_custom_' ) ) {
				$update_data[ $key ] = $value[0];
			}
		}

		/**
		 * Filter the attendee data.
		 *
		 * @param array $update_data The attendee data to sync to the CRM.
		 * @param int   $attendee_id The attendee ID.
		 *
		 * @since 3.37.13
		 */

		$update_data = apply_filters( 'wpf_event_tickets_attendee_data', $update_data, $attendee_id );

		return $update_data;

	}

	/**
	 * Creates / updates a contact record for a single attendee, and applies tags
	 *
	 * @access  public
	 * @return  int Contact ID
	 */

	public function process_attendee( $attendee_id, $apply_tags = array() ) {

		$update_data = $this->get_attendee_meta( $attendee_id );

		$email_address = false;

		foreach ( $update_data as $key => $value ) {
			if ( is_email( $value ) && 'organizer_email' !== $key ) {
				$email_address = $value;
				break;
			}
		}

		if ( false === $email_address ) {
			wpf_log( 'notice', 0, 'Unable to sync event attendee, no email address found:', array( 'meta_array' => $update_data ) );
			return;
		}

		$update_data['user_email'] = $email_address;

		$user = get_user_by( 'email', $email_address );

		if ( ! empty( $user ) ) {

			wp_fusion()->user->push_user_meta( $user->ID, $update_data );

			$contact_id = wp_fusion()->user->get_contact_id( $user->ID );

		} else {

			$contact_id = $this->guest_registration( $email_address, $update_data );

		}

		// Get any dynamic tags out of the update data

		$apply_tags = array_merge( $apply_tags, $this->get_dynamic_tags( $update_data ) );

		if ( ! empty( $apply_tags ) ) {

			if ( ! empty( $user ) ) {

				wp_fusion()->user->apply_tags( $apply_tags, $user->ID );

			} elseif ( ! empty( $contact_id ) ) {

				wpf_log( 'info', 0, 'Applying event tag(s) for guest checkout: ', array( 'tag_array' => $apply_tags ) );
				wp_fusion()->crm->apply_tags( $apply_tags, $contact_id );

			}

		}

		// Save the contact ID to the attendee meta
		update_post_meta( $attendee_id, wp_fusion()->crm->slug . '_contact_id', $contact_id );

		return $contact_id;

	}

	/**
	 * Fires when a ticket is relocated from ticket type to another, which may be in
	 * a different post altogether.
	 *
	 * @param int $ticket_id                the ticket which has been moved
	 * @param int $src_ticket_type_id       the ticket type it belonged to originally
	 * @param int $tgt_ticket_type_id       the ticket type it now belongs to
	 * @param int $src_event_id             the event/post which the ticket originally belonged to
	 * @param int $tgt_event_id             the event/post which the ticket now belongs to
	 * @param int $instigator_id            the user who initiated the change
	 *
	 * @access  public
	 * @return  void
	 */

	public function tickets_ticket_moved( $attendee_id, $src_ticket_type_id, $tgt_ticket_type_id, $src_event_id, $tgt_event_id, $instigator_id ) {

		$attendee_user_id = get_post_meta( $attendee_id, '_tribe_tickets_attendee_user_id', true );
		$contact_id       = get_post_meta( $attendee_id, wp_fusion()->crm->slug . '_contact_id', true );

		if ( empty( $attendee_user_id ) && empty( $contact_id ) ) {
			wpf_log( 'notice', 0, 'Attendee #' . $attendee_id . ' moved from ticket <strong>' . get_the_title( $src_ticket_type_id ) . '</strong> to <strong>' . get_the_title( $tgt_ticket_type_id ) . '</strong> but no user ID or contact ID found for the attendee, so nothing will be synced.' );
			return;
		}

		wpf_log( 'notice', $attendee_user_id, 'Attendee #' . $attendee_id . ' moved from ticket <strong>' . get_the_title( $src_ticket_type_id ) . '</strong> to <strong>' . get_the_title( $tgt_ticket_type_id ) . '</strong>.' );

		// Remove old tags

		if ( get_post_type( $src_ticket_type_id ) == 'download' ) {
			$settings = get_post_meta( $src_ticket_type_id, 'wpf-settings-edd', true );
		} else {
			$settings = get_post_meta( $src_ticket_type_id, 'wpf_settings', true );
		}

		if ( ! empty( $settings ) && ! empty( $settings['apply_tags'] ) ) {

			if ( ! empty( $attendee_user_id ) ) {
				wp_fusion()->user->remove_tags( $settings['apply_tags'], $attendee_user_id );
			} else {
				wp_fusion()->crm->remove_tags( $settings['apply_tags'], $contact_id );
			}
		}

		// Sync meta

		$update_data = $this->get_attendee_meta( $attendee_id );

		if ( ! empty( $attendee_user_id ) ) {
			wp_fusion()->user->push_user_meta( $attendee_user_id, $update_data );
		} else {
			wp_fusion()->crm->update_contact( $contact_id, $update_data );
		}

		// Apply new tags

		if ( get_post_type( $tgt_ticket_type_id ) == 'download' ) {
			$settings = get_post_meta( $tgt_ticket_type_id, 'wpf-settings-edd', true );
		} else {
			$settings = get_post_meta( $tgt_ticket_type_id, 'wpf_settings', true );
		}

		if ( ! empty( $settings ) && ! empty( $settings['apply_tags'] ) ) {

			if ( ! empty( $attendee_user_id ) ) {
				wp_fusion()->user->apply_tags( $settings['apply_tags'], $attendee_user_id );
			} else {
				wp_fusion()->crm->apply_tags( $settings['apply_tags'], $contact_id );
			}
		}

	}


	/**
	 * RSVP ticket created
	 *
	 * @access  public
	 * @return  void
	 */

	public function rsvp_ticket_created( $attendee_id, $post_id, $ticket_id, $order_attendee_id ) {

		// Get settings

		$settings = get_post_meta( $ticket_id, 'wpf_settings', true );

		if ( empty( $settings ) ) {
			$settings = array( 'apply_tags' => array() );
		}

		if ( 1 == $order_attendee_id ) {

			if ( ! empty( $_POST['attendee'] ) ) {

				// Get the attendee info from the POST data for the first order attendee (classic RSVP experience)

				$full_name = $_POST['attendee']['full_name'];
				$email     = $_POST['attendee']['email'];

			} else {

				$full_name = get_post_meta( $attendee_id, '_tribe_rsvp_full_name', true );
				$email     = get_post_meta( $attendee_id, '_tribe_rsvp_email', true );

			}

			$update_data = $this->get_attendee_meta( $attendee_id );

			// Split the full name into two fields

			$names = explode( ' ', $full_name );

			$update_data['first_name'] = $names[0];

			unset( $names[0] );

			if ( ! empty( $names ) ) {

				$update_data['last_name'] = implode( ' ', $names );

			}

			$update_data['user_email'] = $email;

			$user_id = wpf_get_current_user_id();

			if ( false == $user_id ) {

				$user = get_user_by( 'email', $email );

				if ( $user ) {
					$user_id = $user->ID;
				}
			}

			if ( $user_id ) {

				wp_fusion()->user->push_user_meta( $user_id, $update_data );

				$contact_id = wp_fusion()->user->get_contact_id( $user_id );

			} else {

				$contact_id = $this->guest_registration( $email, $update_data );

			}

			if ( ! empty( $settings['apply_tags'] ) ) {

				if ( $user_id ) {

					wp_fusion()->user->apply_tags( $settings['apply_tags'], $user_id );

				} else {

					wpf_log( 'info', $user_id, 'Event Tickets guest RSVP applying tag(s): ', array( 'tag_array' => $settings['apply_tags'] ) );

					wp_fusion()->crm->apply_tags( $settings['apply_tags'], $contact_id );

				}

			}

			update_post_meta( $attendee_id, wp_fusion()->crm->slug . '_contact_id', $contact_id );

		} elseif ( ! empty( $settings['add_attendees'] ) ) {

			// Subsequent attendees, if enabled

			$this->process_attendee( $attendee_id, $settings['apply_tags'] );

		}

	}

	/**
	 * EDD ticket created
	 *
	 * @access  public
	 * @return  void
	 */

	public function edd_ticket_created( $attendee_id, $order_id, $product_id, $order_attendee_id ) {

		$payment = new EDD_Payment( $order_id );

		// We only need to run on the first attendee
		if ( ! empty( $payment->get_meta( '_wpf_tribe_complete', true ) ) ) {
			return;
		}

		$update_data = $this->get_attendee_meta( $attendee_id );

		if ( $payment->user_id > 0 ) {

			wp_fusion()->user->push_user_meta( $payment->user_id, $update_data );

			$contact_id = wp_fusion()->user->get_contact_id( $payment->user_id );

		} else {

			wp_fusion()->crm->update_contact( $contact_id, $update_data );

			$contact_id = $this->guest_registration( $payment->email, $update_data );

		}

		// Save the contact ID to the attendee meta
		update_post_meta( $attendee_id, wp_fusion()->crm->slug . '_contact_id', $contact_id );

		// Mark the order as processed
		$payment->update_meta( '_wpf_tribe_complete', true );

	}

	/**
	 * WooCommerce ticket created
	 *
	 * @access  public
	 * @return  void
	 */

	public function woocommerce_ticket_created( $attendee_id, $order_id, $product_id, $order_attendee_id ) {

		// Get settings
		$ticket_id = get_post_meta( $attendee_id, '_tribe_wooticket_product', true );

		$settings = get_post_meta( $ticket_id, 'wpf_settings', true );

		if ( empty( $settings ) ) {
			$settings = array( 'apply_tags' => array() );
		}

		if ( empty( $settings['add_attendees'] ) && 0 == $order_attendee_id ) {

			// If we're not syncing attendees, then send the data relative to the customer who made the order, just once

			$order       = wc_get_order( $order_id );
			$user_id     = $order->get_user_id();
			$update_data = $this->get_attendee_meta( $attendee_id );

			if ( ! empty( $user_id ) ) {

				wp_fusion()->user->push_user_meta( $user_id, $update_data );

				$contact_id = wp_fusion()->user->get_contact_id( $user_id );

			} else {

				$contact_id = $this->guest_registration( $order->get_billing_email(), $update_data );

			}

			if ( ! empty( $settings['apply_tags'] ) ) {

				if ( ! empty( $user_id ) ) {

					wp_fusion()->user->apply_tags( $settings['apply_tags'], $user_id );

				} elseif ( ! empty( $contact_id ) ) {

					wpf_log( 'info', 0, 'Applying event tag(s) for guest checkout: ', array( 'tag_array' => $settings['apply_tags'] ) );
					wp_fusion()->crm->apply_tags( $settings['apply_tags'], $contact_id );

				}
			}
		} elseif ( ! empty( $settings['add_attendees'] ) ) {

			// If we are syncing attendees

			$this->process_attendee( $attendee_id, $settings['apply_tags'] );

		}

		// Mark the order as processed
		update_post_meta( $order_id, '_wpf_tribe_complete', true );

	}

	/**
	 * Sync checkin status
	 *
	 * @access  public
	 * @return  void
	 */

	public function checkin( $attendee_id, $qr ) {

		$user_id = get_post_meta( $attendee_id, '_tribe_tickets_attendee_user_id', true );

		if ( ! empty( $user_id ) ) {

			wp_fusion()->user->push_user_meta( $user_id, array( 'event_checkin' => true ) );

		} else {

			$contact_id = get_post_meta( $attendee_id, wp_fusion()->crm->slug . '_contact_id', true );

			if ( ! empty( $contact_id ) ) {

				wp_fusion()->crm->update_contact( $contact_id, array( 'event_checkin' => true ) );

			}
		}

	}


	/**
	 * Displays WPF tag option to ticket meta box.
	 *
	 * @access  public
	 * @return  mixed Settings fields
	 */

	public function tickets_metabox( $event_id, $ticket_id ) {

		if ( ! is_admin() || isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'tribe-ticket-edit-Tribe__Tickets_Plus__Commerce__EDD__Main' ) {
			return;
		}

		$settings = array(
			'apply_tags' => array(),
		);

		if ( get_post_meta( $ticket_id, 'wpf_settings', true ) ) {
			$settings = array_merge( $settings, get_post_meta( $ticket_id, 'wpf_settings', true ) );
		}

		/*
		// Apply tags
		*/

		echo '<tr class="ticket wpf-ticket-wrapper' . ( ! empty( $ticket_id ) ? ' has-id' : ' no-id' ) . '" data-id="' . $ticket_id . '">';
		echo '<td>';
		echo '<p><label for="wpf-tet-apply-tags">Apply these tags in ' . wp_fusion()->crm->name . ':</label><br /></p>';
		echo '</td>';
		echo '<td>';

			wpf_render_tag_multiselect(
				array(
					'setting'   => $settings['apply_tags'],
					'meta_name' => 'ticket_wpf_settings',
					'field_id'  => 'apply_tags',
					'class'     => 'ticket_field ' . $ticket_id,
				)
			);

		if ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'tribe-ticket-edit-Tribe__Tickets__RSVP' ) {
			echo '<script type="text/javascript"> initializeTagsSelect("#ticket_form_table"); </script>';
		}

		echo '</td>';
		echo '</tr>';

	}

	/**
	 * Displays WPF tag option to ticket meta box (v4.7.2 and up)
	 *
	 * @access  public
	 * @return  array Field groups
	 */

	public function tickets_metabox_new( $event_id, $ticket_id ) {

		// Don't run on the frontend for Community Events
		if( ! is_admin() ) {
			return;
		}

		$settings = array(
			'apply_tags'    => array(),
			'add_attendees' => false,
		);

		if ( get_post_meta( $ticket_id, 'wpf_settings', true ) ) {
			$settings = array_merge( $settings, get_post_meta( $ticket_id, 'wpf_settings', true ) );
		}

		/*
		// Apply tags
		*/

		echo '<div class="input_block" style="margin: 20px 0;">';

			echo '<label style="width: 132px;" class="ticket_form_label ticket_form_left" for="wpf-tet-apply-tags">' . __( 'Apply tags', 'wp-fusion') . ':</label>';

			wpf_render_tag_multiselect(
				array(
					'setting'   => $settings['apply_tags'],
					'meta_name' => 'ticket_wpf_settings',
					'field_id'  => 'apply_tags',
					'class'     => 'ticket_form_right ticket_field',
				)
			);

			echo '<span class="tribe_soft_note ticket_form_right" style="margin-top: 5px;">' . sprintf( __( 'These tags will be applied in %s when someone RSVPs or purchases this ticket.', 'wp-fusion' ), wp_fusion()->crm->name ) . '</span>';

		echo '</div>';

		echo '<div class="input_block" style="margin: 10px 0 25px;">';
			echo '<label style="width: 132px;" class="ticket_form_label ticket_form_left" for="wpf-add-attendees">' . __( 'Add attendees:', 'wp-fusion' ) . '</label>';
			echo '<input class="checkbox" type="checkbox" style="" id="wpf-add-attendees" name="ticket_wpf_settings[add_attendees]" value="1" ' . checked( $settings['add_attendees'], 1, false ) . ' />';
			echo '<span class="tribe_soft_note">' . sprintf( __( 'Add each event attendee as a separate contact in %s. Requires <a href="https://theeventscalendar.com/knowledgebase/k/collecting-attendee-information/" target="_blank">Individual Attendee Collection</a> to be enabled for this ticket.', 'wp-fusion' ), wp_fusion()->crm->name ) . '</span>';
		echo '</div>';

		if ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'tribe-ticket-edit' ) {
			echo '<script type="text/javascript">initializeTicketTable( ' . $ticket_id . ' );</script>';
		}

	}

	/**
	 * Save meta box data
	 *
	 * @access  public
	 * @return  void
	 */

	public function tickets_after_save_ticket( $post_id, $ticket, $raw_data, $class ) {

		$settings = get_post_meta( $ticket->ID, 'wpf_settings', true );

		if ( empty( $settings ) ) {
			$settings = array();
		}

		if ( isset( $raw_data['ticket_wpf_settings'] ) ) {

			if ( isset( $raw_data['ticket_wpf_settings']['add_attendees'] ) ) {
				$settings['add_attendees'] = true;
			}

			update_post_meta( $ticket->ID, 'wpf_settings', $settings );

		} else {

			if ( ! empty( $settings['add_attendees'] ) ) {

				$settings['add_attendees'] = false;

				update_post_meta( $ticket->ID, 'wpf_settings', $settings );

			}

		}

	}

	/**
	 * Ajax save meta box data (v4.7.2 and up)
	 *
	 * @access  public
	 * @return  void
	 */

	public function ajax_save_ticket() {

		$ticket_id  = $_POST['id'];
		$apply_tags = explode( ',', $_POST['data'] );

		$settings = get_post_meta( $ticket_id, 'wpf_settings', true );

		if ( empty( $settings ) ) {
			$settings = array();
		}

		$settings['apply_tags'] = $apply_tags;

		update_post_meta( $ticket_id, 'wpf_settings', $settings );

		die();

	}

}

new WPF_Tribe_Tickets();
