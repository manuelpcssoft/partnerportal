=== Taggbox Widget: Social Media Aggregator ===
Contributors: Taggbox
Donate link: https://app.taggbox.com/
Tags: Facebook Widget, Instagram Widget, Social Media Aggregator, Social Media Feed, Twitter Widget
Requires PHP: 5.6
Requires at least: 5.1
Tested up to: 5.7
Stable tag: 4.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Taggbox widget is a UGC platform & social media aggregator that helps you create and embed engaging social media feeds, Instagram feed, Twitter feed, Facebook feed and more on the website. 
You can also curate and embed reviews from Google, Yelp, Airbnb, & Facebook on websites. Taggbox widget comes with creative customizations, content filtering, real-time updates, and much more. 
Visit [Taggbox](https://taggbox.com) for more information 

== Plugin Features ==
= Spotlight features =

* Gather content (also UGC) from over 15+ social media and digital platforms
* Customize your feeds with beautiful themes, fonts, layouts, designs, styles, colors, and much more
* (Automatic) Content moderation for your feed to maintain premium content quality
* Make your social content Shoppable with product tagging feature
* Real-time automatic content updates from social media & digital platforms
* Fast and responsive plugin 
* Add custom posts & even modify feeds with custom CSS
* Easy integration with no complex structure or expertise requirement
* In-built analytics to measure the performance of your feeds
* Create photo galleries or video galleries without any hassle
* Active customer support and assistance

=Embed Social Media Feeds=
	Create amazing feeds from social media as Instagram feeds, Tumblr feeds, Twitter feeds, Facebook feeds, etc. 
	[Learn More](https://taggbox.com/social-media-feeds-on-website) [|] [Take Free Trial](https://app.taggbox.com/widget/accounts/register)
    
=Embed Customer Reviews Feeds=
	Create trustworthy & authentic review feeds for websites by curating valuable customer reviews from Airbnb, Google reviews, Yelp, etc. 
	[Learn More](https://taggbox.com/reviews-widget) [|] [Take Free Trial](https://app.taggbox.com/widget/accounts/register)

=Embed Video Feeds=
	Create engaging and interesting video feeds using content from platforms like YouTube, Vimeo, Flickr, etc. 

=Embed Audio/Sound Feeds=
	Curate audio content like playlists, songs, channels, etc. from platforms like Soundcloud, Spotify, etc. and embed it on your website
	[Start Free Trial](https://app.taggbox.com/widget/accounts/register) [|] [Request Demo](https://taggbox.com/demo)


== Installation ==

* Method 1 - Install the Taggbox Widget plugin from WordPress and activate it on your website.
* Method 2 - Download the Taggbox plugin zip files, unzip the files, and then upload it to your WordPress website in the /wp-includes/plugins directory or directly upload it in the plugin section and click on the active button to activate the plugin.
  
  Once you have added the plugin using any of the aforementioned methods. Then you should go to the Taggbox widget plugin and copy the shortcode & embed it on your website where you want to display it.
  [How To Use Taggbox Widget Plugin](https://taggbox.com/support/how-to-use-taggbox-widget-plugin)


== How To Get A Shortcode  ==

* [Register](https://app.taggbox.com/widget/accounts/register) or Log in to the Taggbox Widget plugin.
* Once you log in,  you will see your existing feeds or else you can start with creating your feeds by clicking on the **+Create Widget** button
* To copy your Shortcode, just click on the “Copy” button under any of your created feeds
* Embed it on the page or post where you want to display the feed(s)

== Support ==

If you have any questions or suggestions, please contact us at <support@taggbox.com>.


== Changelog ==
= 1.0 =
*   First release
= 1.1 =
*   Minor Bug Fixes
*	Improved Performance


== Privacy Policy  And Terms of Service ==

* [Privacy Policy](https://taggbox.com/privacy-policy)
* [Terms of Service](https://taggbox.com/terms-of-service)

== Frequently Asked Questions ==

= What Is Taggbox Widget WordPress Plugin? =

It is a dedicated Taggbox widget plugin for the wordpress users that has been designed and created to make it easier to embed social media feeds on the wordpress website.

= How Can I Create A Social Media Feed? =

You need to access the plugin then log in/register for Taggbox account. There you can follow the step-wise on-boarding process to create your social media feed.

= Does The Plugin Require Separate Registration? =

No. You just need to register once. You can do it through the plugin or directly through the website as well.

= Do I Need Seperate Subscription For Plugin? =

No. You don’t need to buy different subscription for accessing the plugin.


== Screenshots ==

1. Example Feeds for Taggbox Widget Plugin.
2. Log in to your Taggbox Widget account.
3. Copy the shortcode of any widget of your choice. If you don't have a widget yet, you can easily create one in your Taggbox Widget dashboard.
4. Paste the shortcode of the widget that was copied. 
5. Example - Social Media Feeds on Website.